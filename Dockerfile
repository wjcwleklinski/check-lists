FROM eclipse-temurin:21-alpine
ENV APP_HOME=/usr/local/list-service-app
ENV JAR_FILE=build/libs/list-service-*.jar
ENV DB_HOST=localhost \
    DB_PORT=5432 \
    DB_NAME="" \
    DB_USER="" \
    DB_PASS="" \
    ISSUER_URI="https://172.18.0.1:8443/realms/checklist-realm" \
    JAVA_TEMPDIR=${APP_HOME}/tmp \
    JAVA_ARGS=""
COPY ${JAR_FILE} ${APP_HOME}/app.jar

USER root
RUN chown -R 1001:0 ${APP_HOME} && mkdir ${JAVA_TEMPDIR} && chmod -R ug+rwX ${APP_HOME}
USER 1001

CMD cd ${APP_HOME} && java -Djava.io.tmpdir=${JAVA_TEMPDIR} ${JAVA_ARGS} -jar -Dspring.datasource.url=jdbc:postgresql://${DB_HOST}:${DB_PORT}/$DB_NAME -Dspring.datasource.username=${DB_USER} -Dspring.datasource.password=${DB_PASS} -Dspring.security.oauth2.resourceserver.jwt.issuer-uri=${ISSUER_URI} app.jar