package com.wjcwleklinski.listservice.common.validation;

import com.wjcwleklinski.listservice.error.ErrorMessage;
import com.wjcwleklinski.listservice.error.exception.NotFoundException;
import com.wjcwleklinski.listservice.model.entity.AssociatedUser;
import com.wjcwleklinski.listservice.model.entity.CheckList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class CheckListValidatorTest {

    private final String USER_ID = "user-id";

    private CheckListValidator validator;

    @BeforeEach
    void setUp() {
        validator = new CheckListValidator();
    }

    @Test
    void validate_userAssignedToCheckList_ok() {
        CheckList checkList = CheckList.builder()
                .name("name")
                .description("description")
                .build();
        checkList.setId(1L);
        checkList.setCode("code");
        AssociatedUser associatedUser = AssociatedUser.builder()
                .userId(USER_ID)
                .checkListId(checkList.getId())
                .checkList(checkList)
                .build();
        checkList.setAssociatedUsers(Collections.singletonList(associatedUser));
        validator.validateUserAssignedToCheckList(USER_ID, checkList);
    }

    @Test
    void validate_userCreatedCheckList_ok() {
        CheckList checkList = CheckList.builder()
                .name("name")
                .description("description")
                .build();
        checkList.setId(1L);
        checkList.setCode("code");
        checkList.setCreatorId(USER_ID);
        validator.validateUserAssignedToCheckList(USER_ID, checkList);
    }

    @Test
    void validate_userNotAssignedToCheckList_error() {
        CheckList checkList = CheckList.builder()
                .name("name")
                .description("description")
                .build();
        checkList.setId(1L);
        checkList.setCode("code");
        checkList.setCreatorId("other-user");
        NotFoundException exception = assertThrows(
                NotFoundException.class,
                () ->  validator.validateUserAssignedToCheckList(USER_ID, checkList)
        );
        assertEquals(exception.getMessage(), ErrorMessage.ENTITY_OF_CODE_NOT_FOUND.getMessage(checkList.getCode()));
    }
}