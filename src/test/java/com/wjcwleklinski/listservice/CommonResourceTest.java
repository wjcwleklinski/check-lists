package com.wjcwleklinski.listservice;


import com.wjcwleklinski.listservice.repository.CheckListRepository;
import com.wjcwleklinski.listservice.repository.EntryRepository;
import com.wjcwleklinski.listservice.repository.FileRepository;
import com.wjcwleklinski.listservice.security.UserAuthentication;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import java.net.URI;
import java.net.URISyntaxException;

import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration(exclude = {
        DataSourceAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class
})
public class CommonResourceTest {

    protected final String USERNAME = "user@user.com";
    protected final String USER_ID = "some-user-identifier";

    @MockBean
    protected CheckListRepository checkListRepository;

    @MockBean
    protected EntryRepository entryRepository;

    @MockBean
    protected FileRepository fileRepository;

    @MockBean
    protected UserAuthentication userAuthentication;

    @LocalServerPort
    private int randomServerPort;

    protected TestRestTemplate restTemplate = new TestRestTemplate();

    @BeforeEach
    void setUp() {
        when(userAuthentication.getUserId()).thenReturn(USER_ID);
        when(userAuthentication.getUsername()).thenReturn(USERNAME);
        restTemplate.getRestTemplate().setRequestFactory(new HttpComponentsClientHttpRequestFactory());
    }

    protected URI getUrl(String path) throws URISyntaxException {
        return new URI("http://localhost:" + randomServerPort + path);
    }
}
