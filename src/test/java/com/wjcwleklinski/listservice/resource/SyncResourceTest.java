package com.wjcwleklinski.listservice.resource;

import com.wjcwleklinski.listservice.CommonResourceTest;
import com.wjcwleklinski.listservice.model.command.*;
import com.wjcwleklinski.listservice.model.command.response.EntitiesSyncResponse;
import com.wjcwleklinski.listservice.model.entity.CheckList;
import com.wjcwleklinski.listservice.model.entity.Entry;
import com.wjcwleklinski.listservice.model.entity.File;
import com.wjcwleklinski.listservice.sync.SyncAction;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class SyncResourceTest extends CommonResourceTest {

    @Test
    public void syncNewChecklists_allSynced() throws URISyntaxException {
        CheckList checkList1 = CheckList.builder()
                .name("name1")
                .description("description1")
                .build();
        checkList1.setCreatorId(USER_ID);
        checkList1.setId(1L);

        CheckList checkList2 = CheckList.builder()
                .name("name2")
                .description("description2")
                .build();
        checkList2.setCreatorId(USER_ID);
        checkList2.setId(2L);
        when(checkListRepository.save(any(CheckList.class))).thenReturn(checkList1, checkList2);

        CheckListSyncCommand checkListSync1 = getCheckListSyncCommand(checkList1, SyncAction.CREATION, "externalId1");
        CheckListSyncCommand checkListSync2 = getCheckListSyncCommand(checkList2, SyncAction.CREATION, "externalId2");

        CheckListsSyncCommand command = new CheckListsSyncCommand();
        command.setCheckLists(Arrays.asList(checkListSync1, checkListSync2));

        HttpEntity<CheckListsSyncCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<EntitiesSyncResponse> response = restTemplate.exchange(getUrl(String.format("/%s/sync/check-lists", USER_ID)), HttpMethod.POST,
                request, EntitiesSyncResponse.class);
        assertEquals(200, response.getStatusCode().value());
        assertNotNull(response.getBody().results().get(0).code());
        assertTrue(response.getBody().results().get(0).isSynchronized());
        assertEquals(response.getBody().results().get(0).externalId(), checkListSync1.externalId());
        assertNotNull(response.getBody().results().get(1).code());
        assertTrue(response.getBody().results().get(1).isSynchronized());
        assertEquals(response.getBody().results().get(1).externalId(), checkListSync2.externalId());
    }

    @Test
    public void syncNewChecklists_singleFileNotFound_oneNotSynced() throws URISyntaxException {
        File file = new File("test.png", "png", new byte[10]);
        file.setCode("imageCode1");
        when(fileRepository.existsByCode(file.getCode())).thenReturn(true);
        CheckList checkList1 = CheckList.builder()
                .name("name1")
                .description("description1")
                .imageCode(file.getCode())
                .build();
        checkList1.setCreatorId(USER_ID);
        checkList1.setId(1L);

        CheckList checkList2 = CheckList.builder()
                .name("name2")
                .description("description2")
                .imageCode("NOT_EXISTS")
                .build();
        checkList2.setCreatorId(USER_ID);
        checkList2.setId(2L);
        when(checkListRepository.save(any(CheckList.class))).thenReturn(checkList1, checkList2);

        CheckListSyncCommand checkListSync1 = getCheckListSyncCommand(checkList1, SyncAction.CREATION, "externalId1");
        CheckListSyncCommand checkListSync2 = getCheckListSyncCommand(checkList2, SyncAction.CREATION, "externalId2");

        CheckListsSyncCommand command = new CheckListsSyncCommand();
        command.setCheckLists(Arrays.asList(checkListSync1, checkListSync2));

        HttpEntity<CheckListsSyncCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<EntitiesSyncResponse> response = restTemplate.exchange(getUrl(String.format("/%s/sync/check-lists", USER_ID)), HttpMethod.POST,
                request, EntitiesSyncResponse.class);
        assertEquals(200, response.getStatusCode().value());
        assertNotNull(response.getBody().results().get(0).code());
        assertTrue(response.getBody().results().get(0).isSynchronized());
        assertEquals(response.getBody().results().get(0).externalId(), checkListSync1.externalId());
        assertNull(response.getBody().results().get(1).code());
        assertFalse(response.getBody().results().get(1).isSynchronized());
        assertEquals(response.getBody().results().get(1).externalId(), checkListSync2.externalId());
    }

    @Test
    public void modifyChecklists_allSynced() throws URISyntaxException {
        File file = new File("test.png", "png", new byte[10]);
        file.setCode("imageCode1");
        when(fileRepository.existsByCode(file.getCode())).thenReturn(true);
        CheckList checkList1 = CheckList.builder()
                .name("name1")
                .description("description1")
                .imageCode(file.getCode())
                .build();
        checkList1.setCode("code1");
        checkList1.setCreatorId(USER_ID);
        checkList1.setId(1L);

        CheckList checkList2 = CheckList.builder()
                .name("name2")
                .description("description2")
                .build();
        checkList2.setCode("code2");
        checkList2.setCreatorId(USER_ID);
        checkList2.setId(2L);
        when(checkListRepository.getByCodeIn(Arrays.asList(checkList1.getCode(), checkList2.getCode()))).thenReturn(Arrays.asList(checkList1, checkList2));
        when(checkListRepository.save(any(CheckList.class))).thenReturn(checkList1, checkList2);
        checkList1.setDescription("modifiedDescription1");
        checkList2.setDescription("modifiedDescription2");

        CheckListSyncCommand checkListSync1 = getCheckListSyncCommand(checkList1, SyncAction.MODIFICATION, "externalId1");
        CheckListSyncCommand checkListSync2 = getCheckListSyncCommand(checkList2, SyncAction.MODIFICATION, "externalId2");

        CheckListsSyncCommand command = new CheckListsSyncCommand();
        command.setCheckLists(Arrays.asList(checkListSync1, checkListSync2));

        HttpEntity<CheckListsSyncCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<EntitiesSyncResponse> response = restTemplate.exchange(getUrl(String.format("/%s/sync/check-lists", USER_ID)), HttpMethod.POST,
                request, EntitiesSyncResponse.class);
        assertEquals(200, response.getStatusCode().value());
        assertNotNull(response.getBody().results().get(0).code());
        assertTrue(response.getBody().results().get(0).isSynchronized());
        assertEquals(response.getBody().results().get(0).externalId(), checkListSync1.externalId());
        assertNotNull(response.getBody().results().get(1).code());
        assertTrue(response.getBody().results().get(1).isSynchronized());
        assertEquals(response.getBody().results().get(1).externalId(), checkListSync2.externalId());
    }

    @Test
    public void modifyChecklists_userNotAssigned_oneSynced() throws URISyntaxException {
        File file = new File("test.png", "png", new byte[10]);
        file.setCode("imageCode1");
        when(fileRepository.existsByCode(file.getCode())).thenReturn(true);
        CheckList checkList1 = CheckList.builder()
                .name("name1")
                .description("description1")
                .imageCode(file.getCode())
                .build();
        checkList1.setCode("code1");
        checkList1.setCreatorId("other-user-id");
        checkList1.setId(1L);

        CheckList checkList2 = CheckList.builder()
                .name("name2")
                .description("description2")
                .build();
        checkList2.setCode("code2");
        checkList2.setCreatorId(USER_ID);
        checkList2.setId(2L);
        when(checkListRepository.getByCodeIn(Arrays.asList(checkList1.getCode(), checkList2.getCode()))).thenReturn(Arrays.asList(checkList1, checkList2));
        when(checkListRepository.save(any(CheckList.class))).thenReturn(checkList1, checkList2);
        checkList1.setDescription("modifiedDescription1");
        checkList2.setDescription("modifiedDescription2");

        CheckListSyncCommand checkListSync1 = getCheckListSyncCommand(checkList1, SyncAction.MODIFICATION, "externalId1");
        CheckListSyncCommand checkListSync2 = getCheckListSyncCommand(checkList2, SyncAction.MODIFICATION, "externalId2");

        CheckListsSyncCommand command = new CheckListsSyncCommand();
        command.setCheckLists(Arrays.asList(checkListSync1, checkListSync2));

        HttpEntity<CheckListsSyncCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<EntitiesSyncResponse> response = restTemplate.exchange(getUrl(String.format("/%s/sync/check-lists", USER_ID)), HttpMethod.POST,
                request, EntitiesSyncResponse.class);
        assertEquals(200, response.getStatusCode().value());
        assertNotNull(response.getBody().results().get(0).code());
        assertFalse(response.getBody().results().get(0).isSynchronized());
        assertEquals(response.getBody().results().get(0).externalId(), checkListSync1.externalId());
        assertNotNull(response.getBody().results().get(1).code());
        assertTrue(response.getBody().results().get(1).isSynchronized());
        assertEquals(response.getBody().results().get(1).externalId(), checkListSync2.externalId());
    }

    @Test
    public void modifyChecklists_fileNotFound_oneSynced() throws URISyntaxException {
        File file = new File("test.png", "png", new byte[10]);
        file.setCode("imageCode1");
        when(fileRepository.existsByCode(file.getCode())).thenReturn(true);
        CheckList checkList1 = CheckList.builder()
                .name("name1")
                .description("description1")
                .imageCode(file.getCode())
                .build();
        checkList1.setCode("code1");
        checkList1.setCreatorId(USER_ID);
        checkList1.setId(1L);

        CheckList checkList2 = CheckList.builder()
                .name("name2")
                .description("description2")
                .imageCode("invalid_code")
                .build();
        checkList2.setCode("code2");
        checkList2.setCreatorId(USER_ID);
        checkList2.setId(2L);
        when(checkListRepository.getByCodeIn(Arrays.asList(checkList1.getCode(), checkList2.getCode()))).thenReturn(Arrays.asList(checkList1, checkList2));
        when(checkListRepository.save(any(CheckList.class))).thenReturn(checkList1, checkList2);
        checkList1.setDescription("modifiedDescription1");
        checkList2.setDescription("modifiedDescription2");

        CheckListSyncCommand checkListSync1 = getCheckListSyncCommand(checkList1, SyncAction.MODIFICATION, "externalId1");
        CheckListSyncCommand checkListSync2 = getCheckListSyncCommand(checkList2, SyncAction.MODIFICATION, "externalId2");

        CheckListsSyncCommand command = new CheckListsSyncCommand();
        command.setCheckLists(Arrays.asList(checkListSync1, checkListSync2));

        HttpEntity<CheckListsSyncCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<EntitiesSyncResponse> response = restTemplate.exchange(getUrl(String.format("/%s/sync/check-lists", USER_ID)), HttpMethod.POST,
                request, EntitiesSyncResponse.class);
        assertEquals(200, response.getStatusCode().value());
        assertNotNull(response.getBody().results().get(0).code());
        assertTrue(response.getBody().results().get(0).isSynchronized());
        assertEquals(response.getBody().results().get(0).externalId(), checkListSync1.externalId());
        assertNotNull(response.getBody().results().get(1).code());
        assertFalse(response.getBody().results().get(1).isSynchronized());
        assertEquals(response.getBody().results().get(1).externalId(), checkListSync2.externalId());
    }

    @Test
    public void modifyChecklists_userNotAssignedAndFileNotFound_noneSynced() throws URISyntaxException {
        File file = new File("test.png", "png", new byte[10]);
        file.setCode("imageCode1");
        when(fileRepository.existsByCode(file.getCode())).thenReturn(true);
        CheckList checkList1 = CheckList.builder()
                .name("name1")
                .description("description1")
                .imageCode(file.getCode())
                .build();
        checkList1.setCode("code1");
        checkList1.setCreatorId("other-user-id");
        checkList1.setId(1L);

        CheckList checkList2 = CheckList.builder()
                .name("name2")
                .description("description2")
                .imageCode("invalid_code")
                .build();
        checkList2.setCode("code2");
        checkList2.setCreatorId(USER_ID);
        checkList2.setId(2L);
        when(checkListRepository.getByCodeIn(Arrays.asList(checkList1.getCode(), checkList2.getCode()))).thenReturn(Arrays.asList(checkList1, checkList2));
        when(checkListRepository.save(any(CheckList.class))).thenReturn(checkList1, checkList2);
        checkList1.setDescription("modifiedDescription1");
        checkList2.setDescription("modifiedDescription2");

        CheckListSyncCommand checkListSync1 = getCheckListSyncCommand(checkList1, SyncAction.MODIFICATION, "externalId1");
        CheckListSyncCommand checkListSync2 = getCheckListSyncCommand(checkList2, SyncAction.MODIFICATION, "externalId2");

        CheckListsSyncCommand command = new CheckListsSyncCommand();
        command.setCheckLists(Arrays.asList(checkListSync1, checkListSync2));

        HttpEntity<CheckListsSyncCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<EntitiesSyncResponse> response = restTemplate.exchange(getUrl(String.format("/%s/sync/check-lists", USER_ID)), HttpMethod.POST,
                request, EntitiesSyncResponse.class);
        assertEquals(200, response.getStatusCode().value());
        assertNotNull(response.getBody().results().get(0).code());
        assertFalse(response.getBody().results().get(0).isSynchronized());
        assertEquals(response.getBody().results().get(0).externalId(), checkListSync1.externalId());
        assertNotNull(response.getBody().results().get(1).code());
        assertFalse(response.getBody().results().get(1).isSynchronized());
        assertEquals(response.getBody().results().get(1).externalId(), checkListSync2.externalId());
    }

    @Test
    public void removeChecklists_checklistNotExists_allSynced() throws URISyntaxException {
        File file = new File("test.png", "png", new byte[10]);
        file.setCode("imageCode1");
        when(fileRepository.existsByCode(file.getCode())).thenReturn(true);
        CheckList checkList1 = CheckList.builder()
                .name("name1")
                .description("description1")
                .imageCode(file.getCode())
                .build();
        checkList1.setCode("code1");
        checkList1.setCreatorId(USER_ID);
        checkList1.setId(1L);

        CheckList checkList2 = CheckList.builder()
                .name("name2")
                .description("description2")
                .build();
        checkList2.setCode("code2");
        checkList2.setCreatorId(USER_ID);
        checkList2.setId(2L);
        when(checkListRepository.getByCodeIn(Arrays.asList(checkList1.getCode(), checkList2.getCode()))).thenReturn(Arrays.asList(checkList1, checkList2));
        when(checkListRepository.getByCode(checkList1.getCode())).thenReturn(Optional.of(checkList1));

        CheckListSyncCommand checkListSync1 = getCheckListSyncCommand(checkList1, SyncAction.REMOVAL, "externalId1");
        CheckListSyncCommand checkListSync2 = getCheckListSyncCommand(checkList2, SyncAction.REMOVAL, "externalId2");

        CheckListsSyncCommand command = new CheckListsSyncCommand();
        command.setCheckLists(Arrays.asList(checkListSync1, checkListSync2));

        HttpEntity<CheckListsSyncCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<EntitiesSyncResponse> response = restTemplate.exchange(getUrl(String.format("/%s/sync/check-lists", USER_ID)), HttpMethod.POST,
                request, EntitiesSyncResponse.class);
        assertEquals(200, response.getStatusCode().value());
        assertNotNull(response.getBody().results().get(0).code());
        assertTrue(response.getBody().results().get(0).isSynchronized());
        assertEquals(response.getBody().results().get(0).externalId(), checkListSync1.externalId());
        assertNotNull(response.getBody().results().get(1).code());
        assertFalse(response.getBody().results().get(1).isSynchronized());
        assertEquals(response.getBody().results().get(1).externalId(), checkListSync2.externalId());
    }

    @Test
    public void syncNewEntries_allSynced() throws URISyntaxException {
        CheckList checkList = CheckList.builder()
                .name("name1")
                .description("description1")
                .build();
        checkList.setCreatorId(USER_ID);
        checkList.setId(1L);
        checkList.setCode("checkList1");

        Entry entry1 = Entry.builder()
                .name("name1")
                .description("description1")
                .priority(Entry.Priority.MEDIUM.name())
                .isSelected(false)
                .checkList(checkList)
                .build();
        entry1.setCreatorId(USER_ID);
        Entry entry2 = Entry.builder()
                .name("name2")
                .description("description2")
                .priority(Entry.Priority.MEDIUM.name())
                .isSelected(false)
                .checkList(checkList)
                .build();
        entry2.setCreatorId(USER_ID);

        EntrySyncCommand entrySync1 = getEntrySync(entry1, SyncAction.CREATION, "ext1");
        EntrySyncCommand entrySync2 = getEntrySync(entry2, SyncAction.CREATION, "ext2");
        EntriesSyncCommand command = new EntriesSyncCommand();
        command.setEntries(Arrays.asList(entrySync1, entrySync2));

        when(checkListRepository.getByCode(checkList.getCode())).thenReturn(Optional.of(checkList));
        when(entryRepository.save(any(Entry.class))).thenReturn(entry1, entry2);

        HttpEntity<EntriesSyncCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<EntitiesSyncResponse> response = restTemplate.exchange(getUrl(String.format("/%s/sync/entries", USER_ID)), HttpMethod.POST,
                request, EntitiesSyncResponse.class);
        assertEquals(200, response.getStatusCode().value());
        assertNotNull(response.getBody().results().get(0).code());
        assertTrue(response.getBody().results().get(0).isSynchronized());
        assertEquals(response.getBody().results().get(0).externalId(), entrySync1.externalId());
        assertNotNull(response.getBody().results().get(1).code());
        assertTrue(response.getBody().results().get(1).isSynchronized());
        assertEquals(response.getBody().results().get(1).externalId(), entrySync2.externalId());
    }

    @Test
    public void syncNewEntries_checkListNotFound_notSynced() throws URISyntaxException {
        CheckList checkList = CheckList.builder()
                .name("name1")
                .description("description1")
                .build();
        checkList.setCreatorId(USER_ID);
        checkList.setId(1L);
        checkList.setCode("checkList1");

        Entry entry1 = Entry.builder()
                .name("name1")
                .description("description1")
                .priority(Entry.Priority.MEDIUM.name())
                .isSelected(false)
                .checkList(checkList)
                .build();
        entry1.setCreatorId(USER_ID);
        Entry entry2 = Entry.builder()
                .name("name2")
                .description("description2")
                .priority(Entry.Priority.MEDIUM.name())
                .isSelected(false)
                .checkList(checkList)
                .build();
        entry2.setCreatorId(USER_ID);

        EntrySyncCommand entrySync1 = getEntrySync(entry1, SyncAction.CREATION, "ext1");
        EntrySyncCommand entrySync2 = getEntrySync(entry2, SyncAction.CREATION, "ext2");
        EntriesSyncCommand command = new EntriesSyncCommand();
        command.setEntries(Arrays.asList(entrySync1, entrySync2));

        when(checkListRepository.getByCode(checkList.getCode())).thenReturn(Optional.empty());
        when(entryRepository.save(any(Entry.class))).thenReturn(entry1, entry2);

        HttpEntity<EntriesSyncCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<EntitiesSyncResponse> response = restTemplate.exchange(getUrl(String.format("/%s/sync/entries", USER_ID)), HttpMethod.POST,
                request, EntitiesSyncResponse.class);
        assertEquals(200, response.getStatusCode().value());
        assertNull(response.getBody().results().get(0).code());
        assertFalse(response.getBody().results().get(0).isSynchronized());
        assertEquals(response.getBody().results().get(0).externalId(), entrySync1.externalId());
        assertNull(response.getBody().results().get(1).code());
        assertFalse(response.getBody().results().get(1).isSynchronized());
        assertEquals(response.getBody().results().get(1).externalId(), entrySync2.externalId());
    }

    @Test
    public void syncNewEntries_singleFileNotFound_oneNotSynced() throws URISyntaxException {
        File file = new File("test.png", "png", new byte[10]);
        file.setCode("imageCode1");
        when(fileRepository.existsByCode(file.getCode())).thenReturn(true);

        CheckList checkList = CheckList.builder()
                .name("name1")
                .description("description1")
                .build();
        checkList.setCreatorId(USER_ID);
        checkList.setId(1L);
        checkList.setCode("checkList1");

        Entry entry1 = Entry.builder()
                .name("name1")
                .description("description1")
                .priority(Entry.Priority.MEDIUM.name())
                .isSelected(false)
                .checkList(checkList)
                .imageCode(file.getCode())
                .build();
        entry1.setCreatorId(USER_ID);
        Entry entry2 = Entry.builder()
                .name("name2")
                .description("description2")
                .priority(Entry.Priority.MEDIUM.name())
                .isSelected(false)
                .checkList(checkList)
                .imageCode("INVALID_FILE")
                .build();
        entry2.setCreatorId(USER_ID);

        EntrySyncCommand entrySync1 = getEntrySync(entry1, SyncAction.CREATION, "ext1");
        EntrySyncCommand entrySync2 = getEntrySync(entry2, SyncAction.CREATION, "ext2");
        EntriesSyncCommand command = new EntriesSyncCommand();
        command.setEntries(Arrays.asList(entrySync1, entrySync2));

        when(checkListRepository.getByCode(checkList.getCode())).thenReturn(Optional.of(checkList));
        when(entryRepository.save(any(Entry.class))).thenReturn(entry1, entry2);

        HttpEntity<EntriesSyncCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<EntitiesSyncResponse> response = restTemplate.exchange(getUrl(String.format("/%s/sync/entries", USER_ID)), HttpMethod.POST,
                request, EntitiesSyncResponse.class);
        assertEquals(200, response.getStatusCode().value());
        assertNotNull(response.getBody().results().get(0).code());
        assertTrue(response.getBody().results().get(0).isSynchronized());
        assertEquals(response.getBody().results().get(0).externalId(), entrySync1.externalId());
        assertNull(response.getBody().results().get(1).code());
        assertFalse(response.getBody().results().get(1).isSynchronized());
        assertEquals(response.getBody().results().get(1).externalId(), entrySync2.externalId());
    }

    @Test
    public void modifyEntries_allSynced() throws URISyntaxException {
        File file = new File("test.png", "png", new byte[10]);
        file.setCode("imageCode1");
        when(fileRepository.existsByCode(file.getCode())).thenReturn(true);

        CheckList checkList = CheckList.builder()
                .name("name1")
                .description("description1")
                .build();
        checkList.setCreatorId(USER_ID);
        checkList.setId(1L);
        checkList.setCode("checkList1");

        Entry entry1 = Entry.builder()
                .name("name1")
                .description("description1")
                .priority(Entry.Priority.MEDIUM.name())
                .isSelected(false)
                .checkList(checkList)
                .imageCode(file.getCode())
                .build();
        entry1.setCode("entry1");
        entry1.setCreatorId(USER_ID);
        Entry entry2 = Entry.builder()
                .name("name2")
                .description("description2")
                .priority(Entry.Priority.MEDIUM.name())
                .isSelected(false)
                .checkList(checkList)
                .imageCode(file.getCode())
                .build();
        entry2.setCode("entry2");
        entry2.setCreatorId(USER_ID);

        EntrySyncCommand entrySync1 = getEntrySync(entry1, SyncAction.MODIFICATION, "ext1");
        EntrySyncCommand entrySync2 = getEntrySync(entry2, SyncAction.MODIFICATION, "ext2");
        EntriesSyncCommand command = new EntriesSyncCommand();
        command.setEntries(Arrays.asList(entrySync1, entrySync2));

        when(checkListRepository.getByCode(checkList.getCode())).thenReturn(Optional.of(checkList));
        when(entryRepository.save(any(Entry.class))).thenReturn(entry1, entry2);
        when(entryRepository.getByCodeIn(Arrays.asList(entry1.getCode(), entry2.getCode()))).thenReturn(Arrays.asList(entry1, entry2));

        HttpEntity<EntriesSyncCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<EntitiesSyncResponse> response = restTemplate.exchange(getUrl(String.format("/%s/sync/entries", USER_ID)), HttpMethod.POST,
                request, EntitiesSyncResponse.class);
        assertEquals(200, response.getStatusCode().value());
        assertNotNull(response.getBody().results().get(0).code());
        assertTrue(response.getBody().results().get(0).isSynchronized());
        assertEquals(response.getBody().results().get(0).externalId(), entrySync1.externalId());
        assertNotNull(response.getBody().results().get(1).code());
        assertTrue(response.getBody().results().get(1).isSynchronized());
        assertEquals(response.getBody().results().get(1).externalId(), entrySync2.externalId());
    }

    @Test
    public void modifyEntries_checklistNotFound_noneSynced() throws URISyntaxException {
        File file = new File("test.png", "png", new byte[10]);
        file.setCode("imageCode1");
        when(fileRepository.existsByCode(file.getCode())).thenReturn(true);

        CheckList checkList = CheckList.builder()
                .name("name1")
                .description("description1")
                .build();
        checkList.setCreatorId(USER_ID);
        checkList.setId(1L);
        checkList.setCode("checkList1");

        Entry entry1 = Entry.builder()
                .name("name1")
                .description("description1")
                .priority(Entry.Priority.MEDIUM.name())
                .isSelected(false)
                .checkList(checkList)
                .imageCode(file.getCode())
                .build();
        entry1.setCode("entry1");
        entry1.setCreatorId(USER_ID);
        Entry entry2 = Entry.builder()
                .name("name2")
                .description("description2")
                .priority(Entry.Priority.MEDIUM.name())
                .isSelected(false)
                .checkList(checkList)
                .imageCode(file.getCode())
                .build();
        entry2.setCode("entry2");
        entry2.setCreatorId(USER_ID);

        EntrySyncCommand entrySync1 = getEntrySync(entry1, SyncAction.MODIFICATION, "ext1");
        EntrySyncCommand entrySync2 = getEntrySync(entry2, SyncAction.MODIFICATION, "ext2");
        EntriesSyncCommand command = new EntriesSyncCommand();
        command.setEntries(Arrays.asList(entrySync1, entrySync2));

        when(checkListRepository.getByCode(checkList.getCode())).thenReturn(Optional.empty());
        when(entryRepository.save(any(Entry.class))).thenReturn(entry1, entry2);
        when(entryRepository.getByCodeIn(Arrays.asList(entry1.getCode(), entry2.getCode()))).thenReturn(Arrays.asList(entry1, entry2));

        HttpEntity<EntriesSyncCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<EntitiesSyncResponse> response = restTemplate.exchange(getUrl(String.format("/%s/sync/entries", USER_ID)), HttpMethod.POST,
                request, EntitiesSyncResponse.class);
        assertEquals(200, response.getStatusCode().value());
        assertNotNull(response.getBody().results().get(0).code());
        assertFalse(response.getBody().results().get(0).isSynchronized());
        assertEquals(response.getBody().results().get(0).externalId(), entrySync1.externalId());
        assertNotNull(response.getBody().results().get(1).code());
        assertFalse(response.getBody().results().get(1).isSynchronized());
        assertEquals(response.getBody().results().get(1).externalId(), entrySync2.externalId());
    }

    @Test
    public void modifyEntries_fileNotFound_oneSynced() throws URISyntaxException {
        File file = new File("test.png", "png", new byte[10]);
        file.setCode("imageCode1");
        when(fileRepository.existsByCode(file.getCode())).thenReturn(true);

        CheckList checkList = CheckList.builder()
                .name("name1")
                .description("description1")
                .build();
        checkList.setCreatorId(USER_ID);
        checkList.setId(1L);
        checkList.setCode("checkList1");

        Entry entry1 = Entry.builder()
                .name("name1")
                .description("description1")
                .priority(Entry.Priority.MEDIUM.name())
                .isSelected(false)
                .checkList(checkList)
                .imageCode(file.getCode())
                .build();
        entry1.setCode("entry1");
        entry1.setCreatorId(USER_ID);
        Entry entry2 = Entry.builder()
                .name("name2")
                .description("description2")
                .priority(Entry.Priority.MEDIUM.name())
                .isSelected(false)
                .checkList(checkList)
                .imageCode("INVALID_FILE")
                .build();
        entry2.setCode("entry2");
        entry2.setCreatorId(USER_ID);

        EntrySyncCommand entrySync1 = getEntrySync(entry1, SyncAction.MODIFICATION, "ext1");
        EntrySyncCommand entrySync2 = getEntrySync(entry2, SyncAction.MODIFICATION, "ext2");
        EntriesSyncCommand command = new EntriesSyncCommand();
        command.setEntries(Arrays.asList(entrySync1, entrySync2));

        when(checkListRepository.getByCode(checkList.getCode())).thenReturn(Optional.of(checkList));
        when(entryRepository.save(any(Entry.class))).thenReturn(entry1, entry2);
        when(entryRepository.getByCodeIn(Arrays.asList(entry1.getCode(), entry2.getCode()))).thenReturn(Arrays.asList(entry1, entry2));

        HttpEntity<EntriesSyncCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<EntitiesSyncResponse> response = restTemplate.exchange(getUrl(String.format("/%s/sync/entries", USER_ID)), HttpMethod.POST,
                request, EntitiesSyncResponse.class);
        assertEquals(200, response.getStatusCode().value());
        assertNotNull(response.getBody().results().get(0).code());
        assertTrue(response.getBody().results().get(0).isSynchronized());
        assertEquals(response.getBody().results().get(0).externalId(), entrySync1.externalId());
        assertNotNull(response.getBody().results().get(1).code());
        assertFalse(response.getBody().results().get(1).isSynchronized());
        assertEquals(response.getBody().results().get(1).externalId(), entrySync2.externalId());
    }

    @Test
    public void removeEntries_allSynced() throws URISyntaxException {
        CheckList checkList = CheckList.builder()
                .name("name1")
                .description("description1")
                .build();
        checkList.setCreatorId(USER_ID);
        checkList.setId(1L);
        checkList.setCode("checkList1");

        Entry entry1 = Entry.builder()
                .name("name1")
                .description("description1")
                .priority(Entry.Priority.MEDIUM.name())
                .isSelected(false)
                .checkList(checkList)
                .build();
        entry1.setCode("entry1");
        entry1.setCreatorId(USER_ID);
        Entry entry2 = Entry.builder()
                .name("name2")
                .description("description2")
                .priority(Entry.Priority.MEDIUM.name())
                .isSelected(false)
                .checkList(checkList)
                .build();
        entry2.setCode("entry2");
        entry2.setCreatorId(USER_ID);

        EntrySyncCommand entrySync1 = getEntrySync(entry1, SyncAction.REMOVAL, "ext1");
        EntrySyncCommand entrySync2 = getEntrySync(entry2, SyncAction.REMOVAL, "ext2");
        EntriesSyncCommand command = new EntriesSyncCommand();
        command.setEntries(Arrays.asList(entrySync1, entrySync2));

        when(checkListRepository.getByCode(checkList.getCode())).thenReturn(Optional.of(checkList));
        when(entryRepository.getByCodeIn(Arrays.asList(entry1.getCode(), entry2.getCode()))).thenReturn(Arrays.asList(entry1, entry2));
        when(entryRepository.getByCode(entry1.getCode())).thenReturn(Optional.of(entry1));
        when(entryRepository.getByCode(entry2.getCode())).thenReturn(Optional.of(entry2));

        HttpEntity<EntriesSyncCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<EntitiesSyncResponse> response = restTemplate.exchange(getUrl(String.format("/%s/sync/entries", USER_ID)), HttpMethod.POST,
                request, EntitiesSyncResponse.class);
        assertEquals(200, response.getStatusCode().value());
        assertNotNull(response.getBody().results().get(0).code());
        assertTrue(response.getBody().results().get(0).isSynchronized());
        assertEquals(response.getBody().results().get(0).externalId(), entrySync1.externalId());
        assertNotNull(response.getBody().results().get(1).code());
        assertTrue(response.getBody().results().get(1).isSynchronized());
        assertEquals(response.getBody().results().get(1).externalId(), entrySync2.externalId());
    }

    @Test
    public void removeEntries_checklistNotFound_noneSynced() throws URISyntaxException {
        CheckList checkList = CheckList.builder()
                .name("name1")
                .description("description1")
                .build();
        checkList.setCreatorId(USER_ID);
        checkList.setId(1L);
        checkList.setCode("checkList1");

        Entry entry1 = Entry.builder()
                .name("name1")
                .description("description1")
                .priority(Entry.Priority.MEDIUM.name())
                .isSelected(false)
                .checkList(checkList)
                .build();
        entry1.setCode("entry1");
        entry1.setCreatorId(USER_ID);
        Entry entry2 = Entry.builder()
                .name("name2")
                .description("description2")
                .priority(Entry.Priority.MEDIUM.name())
                .isSelected(false)
                .checkList(checkList)
                .build();
        entry2.setCode("entry2");
        entry2.setCreatorId(USER_ID);

        EntrySyncCommand entrySync1 = getEntrySync(entry1, SyncAction.REMOVAL, "ext1");
        EntrySyncCommand entrySync2 = getEntrySync(entry2, SyncAction.REMOVAL, "ext2");
        EntriesSyncCommand command = new EntriesSyncCommand();
        command.setEntries(Arrays.asList(entrySync1, entrySync2));

        when(checkListRepository.getByCode(checkList.getCode())).thenReturn(Optional.empty());
        when(entryRepository.getByCodeIn(Arrays.asList(entry1.getCode(), entry2.getCode()))).thenReturn(Arrays.asList(entry1, entry2));
        when(entryRepository.getByCode(entry1.getCode())).thenReturn(Optional.of(entry1));
        when(entryRepository.getByCode(entry2.getCode())).thenReturn(Optional.of(entry2));

        HttpEntity<EntriesSyncCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<EntitiesSyncResponse> response = restTemplate.exchange(getUrl(String.format("/%s/sync/entries", USER_ID)), HttpMethod.POST,
                request, EntitiesSyncResponse.class);
        assertEquals(200, response.getStatusCode().value());
        assertNotNull(response.getBody().results().get(0).code());
        assertFalse(response.getBody().results().get(0).isSynchronized());
        assertEquals(response.getBody().results().get(0).externalId(), entrySync1.externalId());
        assertNotNull(response.getBody().results().get(1).code());
        assertFalse(response.getBody().results().get(1).isSynchronized());
        assertEquals(response.getBody().results().get(1).externalId(), entrySync2.externalId());
    }

    private CheckListSyncCommand getCheckListSyncCommand(CheckList checkList, SyncAction syncAction, String externalId) {
        return new CheckListSyncCommand(
                checkList.getCode(),
                checkList.getName(),
                checkList.getDescription(),
                checkList.getImageCode(),
                syncAction,
                externalId
        );
    }

    private EntrySyncCommand getEntrySync(Entry entry, SyncAction action, String externalId) {
        return new EntrySyncCommand(
                entry.getCode(),
                entry.getName(),
                entry.getDescription(),
                entry.getImageCode(),
                entry.getPriority(),
                entry.getCheckList() != null ? entry.getCheckList().getCode() : null,
                action,
                externalId
        );
    }
}