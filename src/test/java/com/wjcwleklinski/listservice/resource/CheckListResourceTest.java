package com.wjcwleklinski.listservice.resource;

import com.wjcwleklinski.listservice.CommonResourceTest;
import com.wjcwleklinski.listservice.model.command.CheckListCreateCommand;
import com.wjcwleklinski.listservice.model.command.CheckListUpdateCommand;
import com.wjcwleklinski.listservice.model.command.CheckListsDeleteCommand;
import com.wjcwleklinski.listservice.model.entity.CheckList;
import com.wjcwleklinski.listservice.model.entity.Entry;
import com.wjcwleklinski.listservice.model.specification.CheckListSpecification;
import com.wjcwleklinski.listservice.model.view.CheckListDetailsView;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;


public class CheckListResourceTest extends CommonResourceTest {

    @Test
    public void getCheckListsFullResponse_200() throws URISyntaxException {
        CheckList checkList1 = CheckList.builder()
                .name("name1")
                .description("description1")
                .imageCode("image1")
                .build();
        checkList1.setCode("checkList1");
        checkList1.setCreatorId(USER_ID);
        checkList1.setId(1L);
        CheckList checkList2 = CheckList.builder()
                .name("name2")
                .description("description2")
                .imageCode("image2")
                .build();
        checkList2.setCode("checkList2");
        checkList2.setCreatorId(USER_ID);
        checkList2.setId(2L);
        CheckList checkList3 = CheckList.builder()
                .name("name3")
                .description("description3")
                .imageCode("image3")
                .build();
        checkList3.setCode("checkList3");
        checkList3.setCreatorId(USER_ID);
        checkList3.setId(3L);

        List<CheckList> checkLists = Arrays.asList(
                checkList1,
                checkList2,
                checkList3
        );
        when(checkListRepository.findAll(any(CheckListSpecification.class))).thenReturn(checkLists);

        ResponseEntity<List<Map<String, Object>>> response = restTemplate.exchange(getUrl(String.format("/%s/check-lists", USER_ID)), HttpMethod.GET,
                null, new ParameterizedTypeReference<>(){});
        Assertions.assertEquals(200, response.getStatusCodeValue());
        Assertions.assertEquals(response.getBody().get(0).size(), 5);
        Assertions.assertEquals(response.getBody().get(0).get("code"), "checkList1");
        Assertions.assertEquals(response.getBody().get(0).get("name"), "name1");
        Assertions.assertEquals(response.getBody().get(0).get("description"), "description1");
        Assertions.assertEquals(response.getBody().get(0).get("imageUrl"), "https://localhost:8082/files/image1");
        Assertions.assertEquals(response.getBody().get(0).get("creatorId"), USER_ID);
        Assertions.assertEquals(response.getBody().get(1).size(), 5);
        Assertions.assertEquals(response.getBody().get(1).get("code"), "checkList2");
        Assertions.assertEquals(response.getBody().get(1).get("name"), "name2");
        Assertions.assertEquals(response.getBody().get(1).get("description"), "description2");
        Assertions.assertEquals(response.getBody().get(1).get("imageUrl"), "https://localhost:8082/files/image2");
        Assertions.assertEquals(response.getBody().get(1).get("creatorId"), USER_ID);
        Assertions.assertEquals(response.getBody().get(2).size(), 5);
        Assertions.assertEquals(response.getBody().get(2).get("code"), "checkList3");
        Assertions.assertEquals(response.getBody().get(2).get("name"), "name3");
        Assertions.assertEquals(response.getBody().get(2).get("description"), "description3");
        Assertions.assertEquals(response.getBody().get(2).get("imageUrl"), "https://localhost:8082/files/image3");
        Assertions.assertEquals(response.getBody().get(2).get("creatorId"), USER_ID);
    }

    @Test
    public void getChecklistByCodeFullResponse_200() throws URISyntaxException {
        CheckList checkList1 = CheckList.builder()
                .name("name1")
                .description("description1")
                .imageCode("image1")
                .build();
        checkList1.setCode("checkList1");
        checkList1.setCreatorId(USER_ID);
        checkList1.setId(1L);
        Entry entry1 = Entry.builder()
                .name("name1")
                .description("description1")
                .imageCode("image1")
                .priority(Entry.Priority.MEDIUM.toString())
                .checkList(checkList1)
                .build();
        entry1.setId(1L);
        entry1.setCode("entry1");
        Entry entry2 = Entry.builder()
                .name("name2")
                .description("description2")
                .imageCode("image2")
                .priority(Entry.Priority.MEDIUM.toString())
                .checkList(checkList1)
                .build();
        entry2.setId(2L);
        entry2.setCode("entry2");
        checkList1.setEntries(Arrays.asList(entry1, entry2));

        when(checkListRepository.getByCode("checkList1")).thenReturn(Optional.of(checkList1));
        ResponseEntity<CheckListDetailsView> response = restTemplate.exchange(getUrl(String.format("/%s/check-lists/%s", USER_ID, checkList1.getCode())), HttpMethod.GET,
                null, CheckListDetailsView.class);
        Assertions.assertEquals(200, response.getStatusCodeValue());
        CheckListDetailsView responseBody = response.getBody();
        Assertions.assertEquals(checkList1.getCode(), responseBody.getCode());
        Assertions.assertEquals(checkList1.getName(), responseBody.getName());
        Assertions.assertEquals(checkList1.getDescription(), responseBody.getDescription());
        Assertions.assertEquals("https://localhost:8082/files/" + checkList1.getImageCode(), responseBody.getImageUrl());
        Assertions.assertEquals(2, responseBody.getEntries().size());
        Assertions.assertEquals(entry1.getCode(), responseBody.getEntries().get(0).getCode());
        Assertions.assertEquals(entry1.getName(), responseBody.getEntries().get(0).getName());
        Assertions.assertEquals(entry1.getDescription(), responseBody.getEntries().get(0).getDescription());
        Assertions.assertEquals(entry1.getPriority(), responseBody.getEntries().get(0).getPriority());
        Assertions.assertEquals(entry2.getCode(), responseBody.getEntries().get(1).getCode());
        Assertions.assertEquals(entry2.getName(), responseBody.getEntries().get(1).getName());
        Assertions.assertEquals(entry2.getDescription(), responseBody.getEntries().get(1).getDescription());
        Assertions.assertEquals(entry2.getPriority(), responseBody.getEntries().get(1).getPriority());
    }

    @Test
    public void getNotAssociatedChecklistByCode_404() throws URISyntaxException {
        CheckList checkList1 = CheckList.builder()
                .name("name1")
                .description("description1")
                .imageCode("image1")
                .build();
        checkList1.setCode("checkList1");
        checkList1.setCreatorId("some-other-user");
        checkList1.setId(1L);
        Entry entry1 = Entry.builder()
                .name("name1")
                .description("description1")
                .imageCode("image1")
                .priority(Entry.Priority.MEDIUM.toString())
                .checkList(checkList1)
                .build();
        entry1.setId(1L);
        entry1.setCode("entry1");
        Entry entry2 = Entry.builder()
                .name("name2")
                .description("description2")
                .imageCode("image2")
                .priority(Entry.Priority.MEDIUM.toString())
                .checkList(checkList1)
                .build();
        entry2.setId(2L);
        entry2.setCode("entry2");
        checkList1.setEntries(Arrays.asList(entry1, entry2));

        when(checkListRepository.getByCode("checkList1")).thenReturn(Optional.of(checkList1));
        ResponseEntity<CheckListDetailsView> response = restTemplate.exchange(getUrl(String.format("/%s/check-lists/%s", USER_ID, checkList1.getCode())), HttpMethod.GET,
                null, CheckListDetailsView.class);
        Assertions.assertEquals(404, response.getStatusCodeValue());
    }

    @Test
    public void addCheckList_200() throws URISyntaxException {
        CheckList checkList = CheckList.builder()
                .name("name")
                .description("description1")
                .build();
        checkList.setCode("checkList");
        checkList.setCreatorId(USER_ID);
        checkList.setId(1L);
        CheckListCreateCommand command = new CheckListCreateCommand();
        command.setCode(checkList.getCode());
        command.setName(checkList.getName());
        command.setDescription(checkList.getDescription());
        command.setImageCode(checkList.getImageCode());

        when(checkListRepository.save(any(CheckList.class))).thenReturn(checkList);
        HttpEntity<CheckListCreateCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<String> response = restTemplate.exchange(getUrl(String.format("/%s/check-lists", USER_ID)), HttpMethod.POST,
                request, String.class);
        Assertions.assertEquals(201, response.getStatusCodeValue());
    }

    @Test
    public void updateCheckList_200() throws URISyntaxException {
        CheckList checkList1 = CheckList.builder()
                .name("name1")
                .description("description1")
                .build();
        checkList1.setCode("checkList1");
        checkList1.setId(1L);
        checkList1.setCreatorId(USER_ID);
        CheckListUpdateCommand command = new CheckListUpdateCommand();
        command.setName("name1 updated name");
        command.setDescription("description1 updated description");

        when(checkListRepository.getByCode(checkList1.getCode())).thenReturn(Optional.of(checkList1));
        checkList1.setName(command.getName());
        checkList1.setDescription(command.getDescription());
        checkList1.setImageCode(command.getImageCode());
        when(checkListRepository.save(checkList1)).thenReturn(checkList1);
        HttpEntity<CheckListUpdateCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<String> response = restTemplate.exchange(getUrl(String.format("/%s/check-lists/%s", USER_ID, checkList1.getCode())), HttpMethod.PUT,
                request, String.class);
        Assertions.assertEquals(200, response.getStatusCodeValue());
    }

    @Test
    public void updateNotAssociatedCheckList_404() throws URISyntaxException {
        CheckList checkList1 = CheckList.builder()
                .name("name1")
                .description("description1")
                .build();
        checkList1.setCode("checkList1");
        checkList1.setId(1L);
        checkList1.setCreatorId("some-other-user");
        CheckListUpdateCommand command = new CheckListUpdateCommand();
        command.setName("name1 updated name");
        command.setDescription("description1 updated description");

        when(checkListRepository.getByCode(checkList1.getCode())).thenReturn(Optional.of(checkList1));
        checkList1.setName(command.getName());
        checkList1.setDescription(command.getDescription());
        checkList1.setImageCode(command.getImageCode());
        when(checkListRepository.save(checkList1)).thenReturn(checkList1);
        HttpEntity<CheckListUpdateCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<String> response = restTemplate.exchange(getUrl(String.format("/%s/check-lists/%s", USER_ID, checkList1.getCode())), HttpMethod.PUT,
                request, String.class);
        Assertions.assertEquals(404, response.getStatusCodeValue());
    }

    @Test
    public void deleteCheckList_200() throws URISyntaxException {
        CheckList checkList1 = CheckList.builder()
                .name("name1")
                .description("description1")
                .imageCode("image1")
                .build();
        checkList1.setCode("checkList1");
        checkList1.setId(1L);
        checkList1.setCreatorId(USER_ID);
        when(checkListRepository.getByCode(checkList1.getCode())).thenReturn(Optional.of(checkList1));
        ResponseEntity<String> response = restTemplate.exchange(getUrl(String.format("/%s/check-lists/%s", USER_ID, checkList1.getCode())), HttpMethod.DELETE,
                null, String.class);
        Assertions.assertEquals(200, response.getStatusCodeValue());
    }

    @Test
    public void deleteNotAssociatedCheckList_404() throws URISyntaxException {
        CheckList checkList1 = CheckList.builder()
                .name("name1")
                .description("description1")
                .imageCode("image1")
                .build();
        checkList1.setCode("checkList1");
        checkList1.setId(1L);
        checkList1.setCreatorId("some-other-user");
        when(checkListRepository.getByCode(checkList1.getCode())).thenReturn(Optional.of(checkList1));
        ResponseEntity<String> response = restTemplate.exchange(getUrl(String.format("/%s/check-lists/%s", USER_ID, checkList1.getCode())), HttpMethod.DELETE,
                null, String.class);
        Assertions.assertEquals(404, response.getStatusCodeValue());
    }

    @Test
    public void massDeleteCheckLists_200() throws URISyntaxException {
        CheckList checkList1 = CheckList.builder()
                .name("name1")
                .description("description1")
                .imageCode("image1")
                .build();
        checkList1.setCode("checkList1");
        checkList1.setId(1L);
        checkList1.setCreatorId(USER_ID);
        CheckList checkList2 = CheckList.builder()
                .name("name2")
                .description("description2")
                .imageCode("image2")
                .build();
        checkList2.setCode("checkList2");
        checkList2.setId(2L);
        checkList2.setCreatorId(USER_ID);
        when(checkListRepository.getByCode(checkList1.getCode())).thenReturn(Optional.of(checkList1));
        when(checkListRepository.getByCode(checkList2.getCode())).thenReturn(Optional.of(checkList2));
        CheckListsDeleteCommand command = new CheckListsDeleteCommand();
        command.setCodes(Arrays.asList(checkList1.getCode(), checkList2.getCode()));
        HttpEntity<CheckListsDeleteCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<String> response = restTemplate.exchange(getUrl(String.format("/%s/check-lists", USER_ID)), HttpMethod.DELETE,
                request, String.class);
        Assertions.assertEquals(200, response.getStatusCodeValue());
    }

    @Test
    public void massDeleteNotAssociatedCheckList_404() throws URISyntaxException {
        CheckList checkList1 = CheckList.builder()
                .name("name1")
                .description("description1")
                .imageCode("image1")
                .build();
        checkList1.setCode("checkList1");
        checkList1.setId(1L);
        checkList1.setCreatorId("some-other-user");
        CheckList checkList2 = CheckList.builder()
                .name("name2")
                .description("description2")
                .imageCode("image2")
                .build();
        checkList2.setCode("checkList2");
        checkList2.setId(2L);
        checkList2.setCreatorId("some-other-user");
        when(checkListRepository.getByCode(checkList1.getCode())).thenReturn(Optional.of(checkList1));
        when(checkListRepository.getByCode(checkList2.getCode())).thenReturn(Optional.of(checkList2));
        CheckListsDeleteCommand command = new CheckListsDeleteCommand();
        command.setCodes(Arrays.asList(checkList1.getCode(), checkList2.getCode()));
        HttpEntity<CheckListsDeleteCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<String> response = restTemplate.exchange(getUrl(String.format("/%s/check-lists", USER_ID)), HttpMethod.DELETE,
                request, String.class);
        Assertions.assertEquals(404, response.getStatusCodeValue());
    }
}
