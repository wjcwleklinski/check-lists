package com.wjcwleklinski.listservice.resource;

import com.wjcwleklinski.listservice.CommonResourceTest;
import com.wjcwleklinski.listservice.model.command.*;
import com.wjcwleklinski.listservice.model.entity.CheckList;
import com.wjcwleklinski.listservice.model.entity.Entry;
import com.wjcwleklinski.listservice.model.view.CheckListDetailsView;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class EntryResourceTest extends CommonResourceTest {

    @Test
    public void getEntriesFullResponse_200() throws URISyntaxException {
        CheckList checkList = getCheckList();
        checkList.setCreatorId(USER_ID);
        when(checkListRepository.getByCode(checkList.getCode())).thenReturn(Optional.of(checkList));

        ResponseEntity<List<Map<String, Object>>> response = restTemplate.exchange(getUrl(String.format("/%s/check-lists/%s/entries", USER_ID, checkList.getCode())), HttpMethod.GET,
                null, new ParameterizedTypeReference<>(){});
        Assertions.assertEquals(200, response.getStatusCodeValue());
        Assertions.assertEquals(response.getBody().size(), 2);
        Assertions.assertEquals(response.getBody().get(0).size(), 6);
        Assertions.assertEquals(response.getBody().get(0).get("code"), "entryCode1");
        Assertions.assertEquals(response.getBody().get(0).get("name"), "name1");
        Assertions.assertEquals(response.getBody().get(0).get("priority"), Entry.Priority.MEDIUM.name());
        Assertions.assertEquals(response.getBody().get(0).get("imageUrl"), "https://localhost:8082/files/image1");
        Assertions.assertEquals(response.getBody().get(0).get("selected"), false);
        Assertions.assertEquals(response.getBody().get(1).size(), 6);
        Assertions.assertEquals(response.getBody().get(1).get("code"), "entryCode2");
        Assertions.assertEquals(response.getBody().get(1).get("name"), "name2");
        Assertions.assertEquals(response.getBody().get(1).get("priority"), Entry.Priority.HIGH.name());
        Assertions.assertEquals(response.getBody().get(1).get("imageUrl"), "https://localhost:8082/files/image2");
        Assertions.assertEquals(response.getBody().get(1).get("selected"), false);
    }

    @Test
    public void getEntriesUserNotAssociated_404() throws URISyntaxException {
        CheckList checkList = getCheckList();
        when(checkListRepository.getByCode(checkList.getCode())).thenReturn(Optional.of(checkList));

        ResponseEntity<String> response = restTemplate.exchange(getUrl(String.format("/%s/check-lists/%s/entries", USER_ID, checkList.getCode())), HttpMethod.GET,
                null, String.class);
        Assertions.assertEquals(404, response.getStatusCodeValue());
    }
    @Test
    public void updateEntry_200() throws URISyntaxException {
        CheckList checkList = getCheckList();
        checkList.setCreatorId(USER_ID);

        Entry entry = checkList.getEntries().get(0);
        EntryUpdateCommand command = new EntryUpdateCommand();
        command.setName("updated entry name");
        command.setPriority(Entry.Priority.MEDIUM.name());
        command.setDescription("updated entry description");

        when(entryRepository.getByCode(entry.getCode())).thenReturn(Optional.of(entry));
        when(checkListRepository.getByCode(checkList.getCode())).thenReturn(Optional.of(checkList));
        when(entryRepository.save(entry)).thenReturn(entry);
        HttpEntity<EntryUpdateCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<String> response = restTemplate.exchange(getUrl(String.format("/%s/check-lists/%s/entries/%s", USER_ID, checkList.getCode(), entry.getCode())), HttpMethod.PUT,
                request, String.class);
        Assertions.assertEquals(200, response.getStatusCodeValue());
    }

    @Test
    public void updateEntryNotOnList_404() throws URISyntaxException {
        CheckList checkList = getCheckList();
        checkList.setCreatorId(USER_ID);

        Entry entry = checkList.getEntries().get(0);
        EntryUpdateCommand command = new EntryUpdateCommand();
        command.setName("updated entry name");
        command.setPriority(Entry.Priority.MEDIUM.name());
        command.setDescription("updated entry description");

        when(entryRepository.getByCode(entry.getCode())).thenReturn(Optional.of(entry));
        entry.setName(command.getName());
        entry.setDescription(command.getDescription());
        entry.setPriority(command.getPriority());
        when(entryRepository.save(entry)).thenReturn(entry);
        when(checkListRepository.getByCode(checkList.getCode())).thenReturn(Optional.of(checkList));
        HttpEntity<EntryUpdateCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<String> response = restTemplate.exchange(getUrl(String.format("/%s/check-lists/%s/entries/%s", USER_ID, "other-code", entry.getCode())), HttpMethod.PUT,
                request, String.class);
        Assertions.assertEquals(404, response.getStatusCodeValue());
    }

    @Test
    public void updateEntryUserNotAssociated_404() throws URISyntaxException {
        CheckList checkList = getCheckList();
        Entry entry = checkList.getEntries().get(0);
        EntryUpdateCommand command = new EntryUpdateCommand();
        command.setName("updated entry name");
        command.setPriority(Entry.Priority.MEDIUM.name());
        command.setDescription("updated entry description");

        when(entryRepository.getByCode(entry.getCode())).thenReturn(Optional.of(entry));
        entry.setName(command.getName());
        entry.setDescription(command.getDescription());
        entry.setPriority(command.getPriority());
        when(entryRepository.save(entry)).thenReturn(entry);
        HttpEntity<EntryUpdateCommand> request = new HttpEntity<>(command, new HttpHeaders());
        when(checkListRepository.getByCode(checkList.getCode())).thenReturn(Optional.of(checkList));
        ResponseEntity<String> response = restTemplate.exchange(getUrl(String.format("/%s/check-lists/%s/entries/%s", USER_ID, checkList.getCode(), entry.getCode())), HttpMethod.PUT,
                request, String.class);
        Assertions.assertEquals(404, response.getStatusCodeValue());
    }

    @Test
    public void deleteEntry_200() throws URISyntaxException {
        CheckList checkList = getCheckList();
        checkList.setCreatorId(USER_ID);
        Entry entry = checkList.getEntries().get(0);

        EntryDeleteCommand command = new EntryDeleteCommand();
        command.setListCode(checkList.getCode());
        command.setEntryCodes(Collections.singletonList(entry.getCode()));

        when(checkListRepository.getByCode(checkList.getCode())).thenReturn(Optional.of(checkList));
        HttpEntity<EntryDeleteCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<CheckListDetailsView> response = restTemplate.exchange(getUrl(String.format("/%s/check-lists/%s/entries", USER_ID, checkList.getCode())), HttpMethod.DELETE,
                request, CheckListDetailsView.class);
        Assertions.assertEquals(200, response.getStatusCodeValue());
    }

    @Test
    public void deleteEntryUserNotAssociated_404() throws URISyntaxException {
        CheckList checkList = getCheckList();
        Entry entry = checkList.getEntries().get(0);

        EntryDeleteCommand command = new EntryDeleteCommand();
        command.setListCode(checkList.getCode());
        command.setEntryCodes(Collections.singletonList(entry.getCode()));

        when(checkListRepository.getByCode(checkList.getCode())).thenReturn(Optional.of(checkList));
        HttpEntity<EntryDeleteCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<CheckListDetailsView> response = restTemplate.exchange(getUrl(String.format("/%s/check-lists/%s/entries", USER_ID, checkList.getCode())), HttpMethod.DELETE,
                request, CheckListDetailsView.class);
        Assertions.assertEquals(404, response.getStatusCodeValue());
    }

    @Test
    public void massUpdateExistingEntries_200() throws URISyntaxException {
        CheckList checkList = getCheckList();
        checkList.setCreatorId(USER_ID);
        List<EntryUpsert> entries = checkList.getEntries().stream()
                        .map(this::getEntryUpsert)
                        .collect(Collectors.toList());

        EntriesUpdateCommand command = new EntriesUpdateCommand();
        command.setEntries(entries);

        when(checkListRepository.getByCode(checkList.getCode())).thenReturn(Optional.of(checkList));
        when(entryRepository.save(any())).thenReturn(new Entry());
        HttpEntity<EntriesUpdateCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<String> response = restTemplate.exchange(getUrl(String.format("/%s/check-lists/%s/entries", USER_ID, checkList.getCode())), HttpMethod.PATCH,
                request, String.class);
        Assertions.assertEquals(200, response.getStatusCodeValue());
    }

    @Test
    public void massUpdateEntriesAddNew_200() throws URISyntaxException {
        CheckList checkList = getCheckList();
        checkList.setCreatorId(USER_ID);
        List<EntryUpsert> entries = checkList.getEntries().stream()
                .map(this::getEntryUpsert)
                .collect(Collectors.toList());
        EntryUpsert newEntry = new EntryUpsert();
        newEntry.setCode("new-code");
        newEntry.setDescription("new");
        newEntry.setPriority(Entry.Priority.LOW.name());
        newEntry.setName("new");
        newEntry.setSelected(false);
        entries.add(newEntry);

        EntriesUpdateCommand command = new EntriesUpdateCommand();
        command.setEntries(entries);

        when(checkListRepository.getByCode(checkList.getCode())).thenReturn(Optional.of(checkList));
        when(entryRepository.save(any())).thenReturn(new Entry());
        HttpEntity<EntriesUpdateCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<String> response = restTemplate.exchange(getUrl(String.format("/%s/check-lists/%s/entries", USER_ID, checkList.getCode())), HttpMethod.PATCH,
                request, String.class);
        Assertions.assertEquals(200, response.getStatusCodeValue());
    }

    @Test
    public void massUpdateEntryUserNotAssociated_404() throws URISyntaxException {
        CheckList checkList = getCheckList();
        List<EntryUpsert> entries = checkList.getEntries().stream()
                .map(this::getEntryUpsert)
                .collect(Collectors.toList());

        EntriesUpdateCommand command = new EntriesUpdateCommand();
        command.setEntries(entries);

        when(checkListRepository.getByCode(checkList.getCode())).thenReturn(Optional.of(checkList));
        when(entryRepository.save(any())).thenReturn(new Entry());
        HttpEntity<EntriesUpdateCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<String> response = restTemplate.exchange(getUrl(String.format("/%s/check-lists/%s/entries", USER_ID, checkList.getCode())), HttpMethod.PATCH,
                request, String.class);
        Assertions.assertEquals(404, response.getStatusCodeValue());
    }

    @Test
    public void removeSelected_200() throws URISyntaxException {
        CheckList checkList = getCheckList();
        checkList.setCreatorId(USER_ID);
        checkList.getEntries().forEach(entry -> entry.setSelected(true));

        RemoveSelectedEntriesCommand command = new RemoveSelectedEntriesCommand(checkList.getCode(), USER_ID);

        when(checkListRepository.getByCode(checkList.getCode())).thenReturn(Optional.of(checkList));
        when(checkListRepository.save(any())).thenReturn(checkList);
        HttpEntity<RemoveSelectedEntriesCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<CheckListDetailsView> response = restTemplate.exchange(getUrl(String.format("/%s/check-lists/%s/entries/remove-selected", USER_ID, checkList.getCode())),
                HttpMethod.DELETE, request, CheckListDetailsView.class);
        Assertions.assertEquals(200, response.getStatusCodeValue());
    }

    @Test
    public void removeSelectedUserNotAssociated_404() throws URISyntaxException {
        CheckList checkList = getCheckList();
        checkList.getEntries().forEach(entry -> entry.setSelected(true));

        RemoveSelectedEntriesCommand command = new RemoveSelectedEntriesCommand(checkList.getCode(), USER_ID);

        when(checkListRepository.getByCode(checkList.getCode())).thenReturn(Optional.of(checkList));
        when(checkListRepository.save(any())).thenReturn(checkList);
        HttpEntity<RemoveSelectedEntriesCommand> request = new HttpEntity<>(command, new HttpHeaders());
        ResponseEntity<CheckListDetailsView> response = restTemplate.exchange(getUrl(String.format("/%s/check-lists/%s/entries/remove-selected", USER_ID, checkList.getCode())),
                HttpMethod.DELETE, request, CheckListDetailsView.class);
        Assertions.assertEquals(404, response.getStatusCodeValue());
    }

    private CheckList getCheckList() {
        CheckList checkList = new CheckList();
        checkList.setCode("checkListCode");
        checkList.setId(1L);
        Entry entry1 = Entry.builder()
                .priority(Entry.Priority.MEDIUM.name())
                .imageCode("image1")
                .description("description1")
                .name("name1")
                .checkList(checkList)
                .isSelected(false)
                .build();
        entry1.setCode("entryCode1");
        entry1.setId(1L);
        Entry entry2 = Entry.builder()
                .priority(Entry.Priority.HIGH.name())
                .imageCode("image2")
                .description("description2")
                .name("name2")
                .isSelected(false)
                .checkList(checkList)
                .build();
        entry2.setCode("entryCode2");
        entry2.setId(2L);
        checkList.setEntries(new ArrayList<>(Arrays.asList(entry1, entry2)));
        return checkList;
    }

    private EntryUpsert getEntryUpsert(Entry entry) {
        EntryUpsert entryUpsert = new EntryUpsert();
        entryUpsert.setCode(entry.getCode());
        entryUpsert.setDescription("new description");
        entryUpsert.setPriority(Entry.Priority.HIGH.name());
        entryUpsert.setName("new name");
        entryUpsert.setSelected(!entry.isSelected());
        return entryUpsert;
    }
}
