package com.wjcwleklinski.listservice.common.handler;

import com.wjcwleklinski.listservice.error.ErrorMessage;
import com.wjcwleklinski.listservice.error.exception.InternalServerException;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.core.ResolvableType;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

@Component
@RequiredArgsConstructor
public class CommandProcessor {

    private final ListableBeanFactory beanFactory;
    private static final Logger logger = LogManager.getLogger(CommandProcessor.class);

    @SuppressWarnings("unchecked")
    @Transactional
    public <T, R> R process(T command, Class<R> responseClass) {
        long startTime = System.currentTimeMillis();
        try {
            CommandHandler<T, R> commandHandler = (CommandHandler<T, R>) Arrays.stream(beanFactory.getBeanNamesForType(
                    ResolvableType.forClassWithGenerics(CommandHandler.class, command.getClass(), responseClass)))
                    .map(beanFactory::getBean)
                    .findFirst()
                    .orElseThrow(() -> new InternalServerException(ErrorMessage.COMMAND_HANDLER_NOT_FOUND.getMessage(command.getClass())));
            R response = commandHandler.execute(command);
            logger.info("Command processed in: {} s", (System.currentTimeMillis() - startTime) / 1000L);
            return response;
        } catch (ClassCastException classCastException) {
            throw new InternalServerException(ErrorMessage.COMMAND_HANDLER_NOT_FOUND.getMessage(command.getClass()));
        }
    }
}
