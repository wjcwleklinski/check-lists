package com.wjcwleklinski.listservice.common.handler;

public interface CommandHandler<T, R> {

    R execute(T command);

}
