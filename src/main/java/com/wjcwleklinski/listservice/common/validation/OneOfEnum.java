package com.wjcwleklinski.listservice.common.validation;

import jakarta.validation.Constraint;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import jakarta.validation.Payload;
import java.lang.annotation.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Documented
@Constraint(validatedBy = { OneOfEnum.OneOfEnumStringValidator.class, OneOfEnum.OneOfEnumEnumValidator.class })
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface OneOfEnum {

    String message() default "Invalid value";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    Class<? extends Enum<?>> enumClass();

    class OneOfEnumStringValidator implements ConstraintValidator<OneOfEnum, String> {

        List<String> enumValues;
        String message;

        @Override
        public void initialize(OneOfEnum constraintAnnotation) {
            enumValues = new ArrayList<>();
            message = constraintAnnotation.message();
            Class<? extends Enum<?>> enumClass = constraintAnnotation.enumClass();
            Enum<?>[] enumConstants = enumClass.getEnumConstants();
            for (Enum<?> enumConstant: enumConstants) {
                enumValues.add(enumConstant.toString().toUpperCase());
            }
        }

        @Override
        public boolean isValid(String value, ConstraintValidatorContext context) {
            if (!enumValues.contains(value.toUpperCase())) {
                context.disableDefaultConstraintViolation();
                message = "Must be one of: " + enumValues.toString();
                context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
                return false;
            }
            return true;
        }
    }

    class OneOfEnumEnumValidator implements ConstraintValidator<OneOfEnum, Enum<?>> {

        List<Enum<?>> enumValues;
        String message;

        @Override
        public void initialize(OneOfEnum constraintAnnotation) {
            enumValues = new ArrayList<>();
            message = constraintAnnotation.message();
            Class<? extends Enum<?>> enumClass = constraintAnnotation.enumClass();
            Enum<?>[] enumConstants = enumClass.getEnumConstants();
            enumValues.addAll(Arrays.asList(enumConstants));
        }

        @Override
        public boolean isValid(Enum<?> value, ConstraintValidatorContext context) {
            if (!enumValues.contains(value)) {
                context.disableDefaultConstraintViolation();
                message = "Must be one of: " + enumValues.toString();
                context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
                return false;
            }
            return true;
        }
    }
}
