package com.wjcwleklinski.listservice.common.validation;

import com.wjcwleklinski.listservice.error.exception.NotFoundException;
import com.wjcwleklinski.listservice.model.entity.AssociatedUser;
import com.wjcwleklinski.listservice.model.entity.CheckList;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;


@Component
public class CheckListValidator {

    public void validateUserAssignedToCheckList(String userId, CheckList checkList) {
        if (isNotAssigned(userId, checkList)) {
            throw new NotFoundException(checkList.getCode());
        }
    }

    public boolean isNotAssigned(String userId, CheckList checkList) {
        return isNotCreator(userId, checkList) && isNotAssociated(userId, checkList);
    }

    private boolean isNotAssociated(String userId, CheckList checkList) {
        return CollectionUtils.emptyIfNull(checkList.getAssociatedUsers()).stream()
                .map(AssociatedUser::getUserId)
                .noneMatch(userId::equals);
    }

    private boolean isNotCreator(String userId, CheckList checkList) {
        return checkList.getCreatorId() == null || !checkList.getCreatorId().equals(userId);
    }
}
