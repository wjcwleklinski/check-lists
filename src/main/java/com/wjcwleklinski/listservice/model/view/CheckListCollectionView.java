package com.wjcwleklinski.listservice.model.view;

import com.wjcwleklinski.listservice.model.entity.CheckList;
import lombok.Data;

@Data
public class CheckListCollectionView {

    private String code;
    private String description;
    private String name;
    private String imageUrl;
    private String creatorId;

    public static CheckListCollectionView getInstance(CheckList list, String fullFileUrl) {
        return new CheckListCollectionView().fillProperties(list, fullFileUrl);
    }

    protected CheckListCollectionView fillProperties(CheckList list, String fullFileUrl) {
        setCode(list.getCode());
        setName(list.getName());
        setDescription(list.getDescription());
        setImageUrl(fullFileUrl);
        setCreatorId(list.getCreatorId());
        return this;
    }
}
