package com.wjcwleklinski.listservice.model.view;

import com.wjcwleklinski.listservice.model.entity.Entry;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor(staticName = "getInstance")
public class EntryDetailsView extends EntryCollectionView {

    private String description;
    private String checkListCode;
    private String checkListName;
    private String creatorId;

    public static EntryDetailsView getInstance(Entry entry, String fileFullUrl) {
        return getInstance().fillProperties(entry, fileFullUrl);
    }

    protected EntryDetailsView fillProperties(Entry entry, String fileFullUrl) {
        super.fillProperties(entry, fileFullUrl);
        setDescription(entry.getDescription());
        setCheckListCode(entry.getCheckList().getCode());
        setCheckListName(entry.getCheckList().getName());
        setCreatorId(entry.getCreatorId());
        return this;
    }
}
