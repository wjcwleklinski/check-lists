package com.wjcwleklinski.listservice.model.view;

import com.wjcwleklinski.listservice.model.entity.Entry;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
public class EntryCollectionView {

    private String code;
    private String priority;
    private String name;
    private String imageUrl;
    private String creatorId;
    private boolean isSelected;

    public static EntryCollectionView getInstance(Entry entry, String fullFileUrl) {
        return new EntryCollectionView().fillProperties(entry, fullFileUrl);
    }

    protected EntryCollectionView fillProperties(Entry entry, String fullFileUrl) {
        setCode(entry.getCode());
        setPriority(entry.getPriority());
        setName(entry.getName());
        setImageUrl(fullFileUrl);
        setCreatorId(entry.getCreatorId());
        setSelected(entry.isSelected());
        return this;
    }
}
