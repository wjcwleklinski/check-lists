package com.wjcwleklinski.listservice.model.view;

import com.wjcwleklinski.listservice.model.entity.CheckList;
import com.wjcwleklinski.listservice.service.HttpService;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor(staticName = "getInstance", access = AccessLevel.PROTECTED)
public class CheckListDetailsView {

    private String name;
    private String code;
    private String description;
    private String imageUrl;
    private String creatorId;
    private List<EntryDetailsView> entries;

    public static CheckListDetailsView getInstance(CheckList checkList, HttpService httpService) {
        return getInstance().fillProperties(checkList, httpService);
    }

    private CheckListDetailsView fillProperties(CheckList checkList, HttpService httpService) {
        setName(checkList.getName());
        setCode(checkList.getCode());
        setDescription(checkList.getDescription());
        setImageUrl(httpService.getFullFileUrl(checkList.getImageCode()));
        setCreatorId(checkList.getCreatorId());
        setEntries(CollectionUtils.emptyIfNull(checkList.getEntries())
                .stream()
                .map(entry -> EntryDetailsView.getInstance(entry, httpService.getFullFileUrl(entry.getImageCode())))
                .collect(Collectors.toList()));
        return this;
    }
}
