package com.wjcwleklinski.listservice.model.specification;

import com.wjcwleklinski.listservice.model.entity.AssociatedUser;
import com.wjcwleklinski.listservice.model.entity.CheckList;
import com.wjcwleklinski.listservice.model.search.CheckListSearch;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.data.jpa.domain.Specification;

import jakarta.persistence.criteria.*;

import java.util.ArrayList;
import java.util.List;

public class CheckListSpecification implements Specification<CheckList> {

    private CheckListSearch search;

    public CheckListSpecification(CheckListSearch search) {
        super();
        this.search = search;
    }

    @Override
    public Predicate toPredicate(Root<CheckList> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();
        if (search.getCreatorId() != null) {
            predicates.add(criteriaBuilder.equal(root.get("creatorId"), search.getCreatorId()));
        }
        if (CollectionUtils.isNotEmpty(search.getAssociatedUserIds())) {
            Join<CheckList, AssociatedUser> join = root.join("associatedUsers", JoinType.LEFT);
            predicates.add(join.get("userId").in(search.getAssociatedUserIds()));
        }
        return criteriaBuilder.or(predicates.toArray(new Predicate[0]));
    }
}
