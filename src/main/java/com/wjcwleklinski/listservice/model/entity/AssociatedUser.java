package com.wjcwleklinski.listservice.model.entity;

import lombok.*;

import jakarta.persistence.*;

@Entity
@Table(name = "ASU_ASSOCIATED_USERS")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@SequenceGenerator(name = "ASU_SEQUENCE", sequenceName = "ASU_SEQUENCE")
public class AssociatedUser {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ASU_SEQUENCE")
    @Column(name = "ASU_ID")
    private Long id;

    @Column(name = "ASU_CHECKLIST_ID", insertable = false, updatable = false)
    private Long checkListId;

    @ManyToOne
    @JoinColumn(name = "ASU_CHECKLIST_ID")
    private CheckList checkList;

    @Column(name = "ASU_USER_ID")
    private String userId;

    @Column(name = "ASU_USERNAME")
    private String username;
}
