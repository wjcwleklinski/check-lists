package com.wjcwleklinski.listservice.model.entity;

import lombok.*;

import jakarta.persistence.*;


@Entity
@Table(name = "FLE_FILES")
@Getter
@Setter
@Builder
@SequenceGenerator(name = "COMMON_GEN", sequenceName = "FLE_SEQUENCE")
@NoArgsConstructor
@AllArgsConstructor
@AttributeOverrides({
        @AttributeOverride(name = "code", column = @Column(name = "FLE_CODE")),
        @AttributeOverride(name = "id", column = @Column(name = "FLE_ID")),
        @AttributeOverride(name = "creatorId", column = @Column(name = "FLE_CREATOR_ID")),
        @AttributeOverride(name = "creationDate", column = @Column(name = "FLE_CREATION_DATE")),
        @AttributeOverride(name = "modificationTimestamp", column = @Column(name = "FLE_MODIFICATION_TIMESTAMP"))
})
public class File extends CommonEntity {

    @Column(name = "FLE_NAME")
    private String name;

    @Column(name = "FLE_TYPE")
    private String type;

    @Column(name = "FLE_DATA")
    private byte[] data;
}
