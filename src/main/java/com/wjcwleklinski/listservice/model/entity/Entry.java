package com.wjcwleklinski.listservice.model.entity;

import lombok.*;

import jakarta.persistence.*;


@Entity
@Table(name = "ETR_ENTRIES")
@Getter
@Setter
@AttributeOverrides({
        @AttributeOverride(name = "code", column = @Column(name = "ETR_CODE")),
        @AttributeOverride(name = "id", column = @Column(name = "ETR_ID")),
        @AttributeOverride(name = "creatorId", column = @Column(name = "ETR_CREATOR_ID")),
        @AttributeOverride(name = "creationDate", column = @Column(name = "ETR_CREATION_DATE")),
        @AttributeOverride(name = "modificationTimestamp", column = @Column(name = "ETR_MODIFICATION_TIMESTAMP"))
})
@SequenceGenerator(name = "COMMON_GEN", sequenceName = "ETR_SEQUENCE")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Entry extends CommonEntity {

    @RequiredArgsConstructor
    @Getter
    public enum Priority {
        HIGH,
        MEDIUM,
        LOW
    }

    @Column(name = "ETR_PRIORITY")
    private String priority;

    @Column(name = "ETR_NAME")
    private String name;

    @Column(name = "ETR_DESCRIPTION")
    private String description;

    @Column(name = "ETR_IMAGE")
    private String imageCode;

    @Column(name = "ETR_IS_SELECTED")
    private boolean isSelected;

    @ManyToOne
    @JoinColumn(name = "ETR_CHL_ID")
    private CheckList checkList;
}
