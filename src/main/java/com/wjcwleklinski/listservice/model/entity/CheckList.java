package com.wjcwleklinski.listservice.model.entity;

import lombok.*;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "CHL_CHECK_LISTS")
@Getter
@Setter
@AttributeOverrides({
        @AttributeOverride(name = "code", column = @Column(name = "CHL_CODE")),
        @AttributeOverride(name = "id", column = @Column(name = "CHL_ID")),
        @AttributeOverride(name = "creatorId", column = @Column(name = "CHL_CREATOR_ID")),
        @AttributeOverride(name = "creationDate", column = @Column(name = "CHL_CREATION_DATE")),
        @AttributeOverride(name = "modificationTimestamp", column = @Column(name = "CHL_MODIFICATION_TIMESTAMP"))
})
@SequenceGenerator(name = "COMMON_GEN", sequenceName = "CHL_SEQUENCE")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CheckList extends CommonEntity {

    @Column(name = "CHL_NAME")
    private String name;

    @Column(name = "CHL_DESCRIPTION")
    private String description;

    @Column(name = "CHL_IMAGE")
    private String imageCode;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "ETR_CHL_ID", referencedColumnName = "CHL_ID")
    private List<Entry> entries;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "checkList")
    private List<AssociatedUser> associatedUsers;
}
