package com.wjcwleklinski.listservice.model.projection;

public interface CheckListCollectionProjection {

    Long getId();

    String getCode();

    String getName();

    String getDescription();

    String getImageCode();

    String getCreatorId();
}
