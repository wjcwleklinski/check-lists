package com.wjcwleklinski.listservice.model.search;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CheckListSearch {

    private String creatorId;
    private List<String> associatedUserIds;
}
