package com.wjcwleklinski.listservice.model.command.handler;

import com.wjcwleklinski.listservice.common.handler.CommandHandler;
import com.wjcwleklinski.listservice.model.entity.Entry;
import com.wjcwleklinski.listservice.model.command.EntryUpdateCommand;
import com.wjcwleklinski.listservice.repository.EntryRepository;
import com.wjcwleklinski.listservice.service.CheckListService;
import com.wjcwleklinski.listservice.service.CommonService;
import com.wjcwleklinski.listservice.service.FileService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
public class EntryUpdateCommandHandler implements CommandHandler<EntryUpdateCommand, String> {

    private final CommonService commonService;
    private final EntryRepository entryRepository;
    private final FileService fileService;
    private final CheckListService checkListService;

    @Override
    public String execute(EntryUpdateCommand command) {
        checkListService.loadCheckList(command.getListCode(), command.getUserId());
        Entry entry = (Entry) commonService.getByCode(command.getEntryCode(), entryRepository);
        entry.setName(command.getName());
        entry.setDescription(command.getDescription());
        entry.setPriority(command.getPriority());
        fileService.validateFileExistence(command.getImageCode());
        entry.setImageCode(command.getImageCode());
        return commonService.save(entry, entryRepository);
    }
}
