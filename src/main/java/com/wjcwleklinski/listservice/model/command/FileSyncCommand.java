package com.wjcwleklinski.listservice.model.command;

import com.wjcwleklinski.listservice.common.validation.OneOfEnum;
import com.wjcwleklinski.listservice.sync.SyncAction;
import com.wjcwleklinski.listservice.sync.Syncable;
import jakarta.validation.constraints.NotBlank;
import org.springframework.web.multipart.MultipartFile;

public record FileSyncCommand(
        String code,
        MultipartFile file,
        @OneOfEnum(enumClass = SyncAction.class) SyncAction action,
        @NotBlank String externalId
) implements Syncable {
}
