package com.wjcwleklinski.listservice.model.command;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;


@Data
public class CheckListUpdateCommand {

    @JsonIgnore
    private String listCode;
    @JsonIgnore
    private String userId;
    private String name;
    private String description;
    private String imageCode;
}
