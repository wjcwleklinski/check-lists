package com.wjcwleklinski.listservice.model.command.handler;

import com.wjcwleklinski.listservice.common.handler.CommandHandler;
import com.wjcwleklinski.listservice.common.validation.CheckListValidator;
import com.wjcwleklinski.listservice.model.entity.CheckList;
import com.wjcwleklinski.listservice.model.command.CheckListUpdateCommand;
import com.wjcwleklinski.listservice.repository.CheckListRepository;
import com.wjcwleklinski.listservice.service.CommonService;
import com.wjcwleklinski.listservice.service.FileService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CheckListUpdateCommandHandler implements CommandHandler<CheckListUpdateCommand, String> {

    private final CommonService commonService;
    private final CheckListRepository checkListRepository;
    private final FileService fileService;
    private final CheckListValidator validator;

    @Override
    public String execute(CheckListUpdateCommand command) {
        CheckList checkList = (CheckList) commonService.getByCode(command.getListCode(), checkListRepository);
        validator.validateUserAssignedToCheckList(command.getUserId(), checkList);
        checkList.setName(command.getName());
        checkList.setDescription(command.getDescription());
        fileService.validateFileExistence(command.getImageCode());
        checkList.setImageCode(command.getImageCode());
        return commonService.save(checkList, checkListRepository);
    }
}
