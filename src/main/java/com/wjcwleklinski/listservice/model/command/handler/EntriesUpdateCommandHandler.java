package com.wjcwleklinski.listservice.model.command.handler;

import com.wjcwleklinski.listservice.common.handler.CommandHandler;
import com.wjcwleklinski.listservice.model.command.EntriesUpdateCommand;
import com.wjcwleklinski.listservice.model.command.EntryUpsert;
import com.wjcwleklinski.listservice.model.entity.Entry;
import com.wjcwleklinski.listservice.model.entity.CheckList;
import com.wjcwleklinski.listservice.repository.EntryRepository;
import com.wjcwleklinski.listservice.service.CheckListService;
import com.wjcwleklinski.listservice.service.CommonService;
import com.wjcwleklinski.listservice.service.FileService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class EntriesUpdateCommandHandler implements CommandHandler<EntriesUpdateCommand, Void> {

    private final CommonService commonService;
    private final CheckListService checkListService;
    private final EntryRepository entryRepository;
    private final FileService fileService;

    @Override
    public Void execute(EntriesUpdateCommand command) {
        CheckList checkList = checkListService.loadCheckList(command.getListCode(), command.getUserId());
        for (EntryUpsert entryUpsert : command.getEntries()) {
            Optional<Entry> existingEntry = checkList.getEntries().stream()
                    .filter(entry -> entry.getCode().equals(entryUpsert.getCode()))
                    .findFirst();
            if (existingEntry.isPresent()) {
                modifyExistingEntry(existingEntry.get(), entryUpsert);
            } else {
                addNewEntry(checkList, entryUpsert);
            }
        }
       return null;
    }

    private void modifyExistingEntry(Entry entry, EntryUpsert entryUpsert) {
        fillProperties(entry, entryUpsert);
        commonService.save(entry, entryRepository);
    }

    private void addNewEntry(CheckList checkList, EntryUpsert entryUpsert) {
        Entry entry = new Entry();
        entry.setCode(entryUpsert.getCode());
        fillProperties(entry, entryUpsert);
        entry.setCheckList(checkList);
        if (checkList.getEntries() == null) {
            checkList.setEntries(new ArrayList<>());
        }
        checkList.getEntries().add(entry);
        commonService.save(entry, entryRepository);
    }

    private void fillProperties(Entry entry, EntryUpsert entryUpsert) {
        entry.setName(entryUpsert.getName());
        entry.setDescription(entryUpsert.getDescription());
        entry.setPriority(entryUpsert.getPriority());
        fileService.validateFileExistence(entryUpsert.getImageCode());
        entry.setImageCode(entryUpsert.getImageCode());
        entry.setSelected(entryUpsert.isSelected());
    }
}
