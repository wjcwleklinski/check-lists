package com.wjcwleklinski.listservice.model.command;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class RemoveSelectedEntriesCommand {

    private String listCode;
    private String userId;
}
