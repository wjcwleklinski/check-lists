package com.wjcwleklinski.listservice.model.command.handler;

import com.wjcwleklinski.listservice.common.handler.CommandHandler;
import com.wjcwleklinski.listservice.model.command.PatchSelectedEntriesCommand;
import com.wjcwleklinski.listservice.model.command.SelectedEntry;
import com.wjcwleklinski.listservice.model.entity.CheckList;
import com.wjcwleklinski.listservice.repository.CheckListRepository;
import com.wjcwleklinski.listservice.service.CommonService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
public class PatchSelectedEntriesCommandHandler implements CommandHandler<PatchSelectedEntriesCommand, String> {

    private final CommonService commonService;
    private final CheckListRepository checkListRepository;

    @Override
    public String execute(PatchSelectedEntriesCommand command) {
        CheckList checkList = (CheckList) commonService.getByCode(command.getListCode(), checkListRepository);
        for (SelectedEntry selectedEntry : command.getSelectedEntries()) {
            CollectionUtils.emptyIfNull(checkList.getEntries()).stream()
                    .filter(entry -> entry.getCode().equals(selectedEntry.getEntryCode()))
                    .findFirst()
                    .ifPresent(entry -> entry.setSelected(selectedEntry.isSelected()));
        }
        return commonService.save(checkList, checkListRepository);
    }
}
