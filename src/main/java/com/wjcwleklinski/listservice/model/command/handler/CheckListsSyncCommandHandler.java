package com.wjcwleklinski.listservice.model.command.handler;

import com.wjcwleklinski.listservice.common.handler.CommandHandler;
import com.wjcwleklinski.listservice.common.validation.CheckListValidator;
import com.wjcwleklinski.listservice.model.command.CheckListSyncCommand;
import com.wjcwleklinski.listservice.model.command.response.EntitiesSyncResponse;
import com.wjcwleklinski.listservice.model.command.response.EntitySyncResponse;
import com.wjcwleklinski.listservice.model.command.CheckListsSyncCommand;
import com.wjcwleklinski.listservice.model.entity.CheckList;
import com.wjcwleklinski.listservice.repository.CheckListRepository;
import com.wjcwleklinski.listservice.security.UserAuthentication;
import com.wjcwleklinski.listservice.service.CommonService;
import com.wjcwleklinski.listservice.service.FileService;
import com.wjcwleklinski.listservice.sync.SyncCommandHandler;
import com.wjcwleklinski.listservice.sync.SyncContext;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class CheckListsSyncCommandHandler extends SyncCommandHandler<CheckList, CheckListSyncCommand> implements CommandHandler<CheckListsSyncCommand, EntitiesSyncResponse> {

    @Getter(value = AccessLevel.PROTECTED)
    private final CommonService commonService;
    private final CheckListRepository checkListRepository;
    private final CheckListValidator checkListValidator;
    private final FileService fileService;
    private final UserAuthentication userAuthentication;

    @Override
    public EntitiesSyncResponse execute(CheckListsSyncCommand command) {
        List<EntitySyncResponse> checkListsResponse = processCheckLists(command);
        return new EntitiesSyncResponse(checkListsResponse);
    }

    @Override
    protected Optional<CheckList> findEntityInList(List<CheckList> entities, CheckListSyncCommand command) {
        return entities.stream()
                .filter(ch -> command.code().equals(ch.getCode()))
                .findFirst();
    }

    private CheckList createChecklist(CheckListSyncCommand command) {
        return CheckList.builder()
                .name(command.name())
                .description(command.description())
                .imageCode(command.imageCode())
                .build();
    }

    private CheckList modifyCheckList(CheckList entity, CheckListSyncCommand command) {
        entity.setName(command.name());
        entity.setDescription(command.description());
        entity.setImageCode(command.imageCode());
        entity.setModificationTimestamp(LocalDateTime.now());
        return entity;
    }

    private List<EntitySyncResponse> processCheckLists(CheckListsSyncCommand command) {
        List<CheckList> checkLists = loadCheckLists(command, checkListRepository);
        SyncContext<CheckList, CheckListSyncCommand> syncContext = new SyncContext<>(
                checkLists,
                checkListRepository,
                command.getCheckLists(),
                this::createChecklist,
                this::modifyCheckList,
                this::validateCreation,
                this::validateModification,
                this::validateRemoval
        );
        return synchronize(syncContext);
    }

    private List<CheckList> loadCheckLists(CheckListsSyncCommand command, CheckListRepository checkListRepository) {
        List<String> codes = command.getCheckLists().stream()
                .map(CheckListSyncCommand::code)
                .toList();
        return commonService.getByCodes(codes, checkListRepository);
    }

    private void validateCreation(CheckListSyncCommand command) {
        fileService.validateFileExistence(command.imageCode());
    }

    private void validateModification(CheckList checkList, CheckListSyncCommand command) {
        String userId = userAuthentication.getUserId();
        checkListValidator.validateUserAssignedToCheckList(userId, checkList);
        fileService.validateFileExistence(command.imageCode());
    }

    private void validateRemoval(CheckList checkList, CheckListSyncCommand command) {
        String userId = userAuthentication.getUserId();
        checkListValidator.validateUserAssignedToCheckList(userId, checkList);
    }
}
