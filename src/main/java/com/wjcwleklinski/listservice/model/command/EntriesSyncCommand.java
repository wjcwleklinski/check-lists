package com.wjcwleklinski.listservice.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.List;

@Data
public final class EntriesSyncCommand {
    @JsonIgnore
    private String userId;
    private List<EntrySyncCommand> entries;
}
