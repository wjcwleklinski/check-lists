package com.wjcwleklinski.listservice.model.command.handler;

import com.wjcwleklinski.listservice.common.handler.CommandHandler;
import com.wjcwleklinski.listservice.common.validation.CheckListValidator;
import com.wjcwleklinski.listservice.model.command.CheckListsDeleteCommand;
import com.wjcwleklinski.listservice.model.entity.CheckList;
import com.wjcwleklinski.listservice.repository.CheckListRepository;
import com.wjcwleklinski.listservice.service.CommonService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CheckListsDeleteCommandHandler implements CommandHandler<CheckListsDeleteCommand, Void> {

    private final CommonService commonService;
    private final CheckListRepository checkListRepository;
    private final CheckListValidator validator;

    @Override
    public Void execute(CheckListsDeleteCommand command) {
        for (String code: command.getCodes()) {
            CheckList checkList = (CheckList) commonService.getByCode(code, checkListRepository);
            validator.validateUserAssignedToCheckList(command.getUserId(), checkList);
            commonService.deleteByCode(code, checkListRepository);
        }
        return null;
    }
}
