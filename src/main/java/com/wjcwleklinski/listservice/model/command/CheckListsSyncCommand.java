package com.wjcwleklinski.listservice.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.Valid;
import lombok.Data;

import java.util.List;

@Data
public final class CheckListsSyncCommand {
    @JsonIgnore
    private String userId;
    @Valid
    private List<CheckListSyncCommand> checkLists;
}
