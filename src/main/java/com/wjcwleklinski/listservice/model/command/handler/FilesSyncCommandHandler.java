package com.wjcwleklinski.listservice.model.command.handler;

import com.wjcwleklinski.listservice.common.handler.CommandHandler;
import com.wjcwleklinski.listservice.error.ErrorMessage;
import com.wjcwleklinski.listservice.error.exception.BadRequestException;
import com.wjcwleklinski.listservice.model.command.FileSyncCommand;
import com.wjcwleklinski.listservice.model.command.FilesSyncCommand;
import com.wjcwleklinski.listservice.model.command.response.EntitiesSyncResponse;
import com.wjcwleklinski.listservice.model.command.response.EntitySyncResponse;
import com.wjcwleklinski.listservice.model.entity.File;
import com.wjcwleklinski.listservice.repository.FileRepository;
import com.wjcwleklinski.listservice.service.CommonService;
import com.wjcwleklinski.listservice.sync.SyncCommandHandler;
import com.wjcwleklinski.listservice.sync.SyncContext;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class FilesSyncCommandHandler extends SyncCommandHandler<File, FileSyncCommand> implements CommandHandler<FilesSyncCommand, EntitiesSyncResponse> {

    private static final Logger logger = LogManager.getLogger(FilesSyncCommandHandler.class);

    @Getter(value = AccessLevel.PROTECTED)
    private final CommonService commonService;
    private final FileRepository fileRepository;

    @Override
    public EntitiesSyncResponse execute(FilesSyncCommand command) {
        List<EntitySyncResponse> filesResponse = processFiles(command);
        return new EntitiesSyncResponse(filesResponse);
    }

    @Override
    protected Optional<File> findEntityInList(List<File> entities, FileSyncCommand command) {
        return entities.stream()
                .filter(ch -> command.code().equals(ch.getCode()))
                .findFirst();
    }

    private File createFile(FileSyncCommand command) {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(command.file().getOriginalFilename()));
        try {
            return File.builder()
                    .name(fileName)
                    .type(command.file().getContentType())
                    .data(command.file().getBytes())
                    .build();
        } catch (IOException e) {
            logger.error("I/O exception for file: {}", fileName);
            throw new RuntimeException(e);
        }
    }

    private List<EntitySyncResponse> processFiles(FilesSyncCommand command) {
        List<File> files = loadFiles(command, fileRepository);
        SyncContext<File, FileSyncCommand> syncContext = new SyncContext<>(
                files,
                fileRepository,
                command.getFiles(),
                this::createFile,
                (e, c) -> null,
                (c) -> {},
                this::validateModify,
                (e, c) -> {}
        );
        return synchronize(syncContext);
    }

    private List<File> loadFiles(FilesSyncCommand command, FileRepository fileRepository) {
        List<String> codes = command.getFiles().stream()
                .map(FileSyncCommand::code)
                .toList();
        return commonService.getByCodes(codes, fileRepository);
    }

    private void validateModify(File file, FileSyncCommand command) {
        throw new BadRequestException(ErrorMessage.INVALID_SYNC_ACTION.getMessage());
    }
}
