package com.wjcwleklinski.listservice.model.command;


import com.wjcwleklinski.listservice.common.validation.OneOfEnum;
import com.wjcwleklinski.listservice.sync.SyncAction;
import com.wjcwleklinski.listservice.sync.Syncable;
import jakarta.validation.constraints.NotBlank;


public record CheckListSyncCommand(
        String code,
        String name,
        String description,
        String imageCode,
        @OneOfEnum(enumClass = SyncAction.class) SyncAction action,
        @NotBlank String externalId
) implements Syncable {
}
