package com.wjcwleklinski.listservice.model.command.response;

import java.util.List;

public record EntitiesSyncResponse(
        List<EntitySyncResponse> results
) {
}
