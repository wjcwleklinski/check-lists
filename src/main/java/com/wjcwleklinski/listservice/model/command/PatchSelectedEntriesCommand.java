package com.wjcwleklinski.listservice.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import jakarta.validation.Valid;
import java.util.List;

@Data
public class PatchSelectedEntriesCommand {

    @JsonIgnore
    private String listCode;
    @Valid
    private List<SelectedEntry> selectedEntries;
}
