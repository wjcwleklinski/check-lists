package com.wjcwleklinski.listservice.model.command.response;


public record EntitySyncResponse(
        String code,
        String externalId,
        boolean isSynchronized
) {
}
