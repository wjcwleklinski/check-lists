package com.wjcwleklinski.listservice.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.List;

@Data
public class CheckListsDeleteCommand {

    private List<String> codes;
    @JsonIgnore
    private String userId;
}
