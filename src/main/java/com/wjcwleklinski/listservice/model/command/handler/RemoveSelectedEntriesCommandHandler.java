package com.wjcwleklinski.listservice.model.command.handler;

import com.wjcwleklinski.listservice.common.handler.CommandHandler;
import com.wjcwleklinski.listservice.model.command.RemoveSelectedEntriesCommand;
import com.wjcwleklinski.listservice.model.entity.CheckList;
import com.wjcwleklinski.listservice.model.entity.Entry;
import com.wjcwleklinski.listservice.repository.CheckListRepository;
import com.wjcwleklinski.listservice.service.CheckListService;
import com.wjcwleklinski.listservice.service.CommonService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
public class RemoveSelectedEntriesCommandHandler implements CommandHandler<RemoveSelectedEntriesCommand, String> {

    private final CommonService commonService;
    private final CheckListRepository checkListRepository;
    private final CheckListService checkListService;

    @Override
    public String execute(RemoveSelectedEntriesCommand command) {
        CheckList checkList = checkListService.loadCheckList(command.getListCode(), command.getUserId());
        CollectionUtils.emptyIfNull(checkList.getEntries())
                .removeIf(Entry::isSelected);
        return commonService.save(checkList, checkListRepository);
    }
}
