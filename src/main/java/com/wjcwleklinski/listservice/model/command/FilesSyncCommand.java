package com.wjcwleklinski.listservice.model.command;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class FilesSyncCommand {
    private String userId;
    private List<FileSyncCommand> files;
}
