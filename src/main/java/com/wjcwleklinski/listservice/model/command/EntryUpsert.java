package com.wjcwleklinski.listservice.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wjcwleklinski.listservice.model.entity.Entry;
import com.wjcwleklinski.listservice.common.validation.OneOfEnum;
import lombok.Data;

import jakarta.validation.constraints.NotBlank;

@Data
public class EntryUpsert {

    private String code;

    @NotBlank
    @OneOfEnum(enumClass = Entry.Priority.class)
    private String priority;

    @NotBlank
    private String name;

    private String description;

    private String imageCode;

    private boolean selected;
}
