package com.wjcwleklinski.listservice.model.command.handler;

import com.wjcwleklinski.listservice.common.handler.CommandHandler;
import com.wjcwleklinski.listservice.model.entity.AssociatedUser;
import com.wjcwleklinski.listservice.model.entity.CheckList;
import com.wjcwleklinski.listservice.model.command.CheckListCreateCommand;
import com.wjcwleklinski.listservice.repository.CheckListRepository;
import com.wjcwleklinski.listservice.security.UserAuthentication;
import com.wjcwleklinski.listservice.service.CommonService;
import com.wjcwleklinski.listservice.service.FileService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
@RequiredArgsConstructor
public class CheckListCreateCommandHandler implements CommandHandler<CheckListCreateCommand, String> {

    private final CommonService commonService;
    private final CheckListRepository checkListRepository;
    private final FileService fileService;
    private final UserAuthentication userAuthentication;

    @Override
    public String execute(CheckListCreateCommand command) {
        if (StringUtils.isNotBlank(command.getImageCode())) {
            fileService.getFile(command.getImageCode());
        }
        CheckList checkList = CheckList.builder()
                .name(command.getName())
                .description(command.getDescription())
                .imageCode(command.getImageCode())
                .build();
        checkList.setCode(command.getCode());
        AssociatedUser association = AssociatedUser.builder()
                .checkList(checkList)
                .userId(userAuthentication.getUserId())
                .username(userAuthentication.getUsername())
                .build();
        checkList.setAssociatedUsers(Collections.singletonList(association));
        return commonService.save(checkList, checkListRepository);
    }
}
