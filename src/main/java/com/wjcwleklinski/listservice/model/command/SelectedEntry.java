package com.wjcwleklinski.listservice.model.command;

import lombok.Data;

import jakarta.validation.constraints.NotNull;

@Data
public class SelectedEntry {

    @NotNull
    private String entryCode;
    @NotNull
    private boolean selected;
}
