package com.wjcwleklinski.listservice.model.command.handler;

import com.wjcwleklinski.listservice.common.handler.CommandHandler;
import com.wjcwleklinski.listservice.model.command.*;
import com.wjcwleklinski.listservice.model.command.response.EntitiesSyncResponse;
import com.wjcwleklinski.listservice.model.command.response.EntitySyncResponse;
import com.wjcwleklinski.listservice.model.entity.CheckList;
import com.wjcwleklinski.listservice.model.entity.Entry;
import com.wjcwleklinski.listservice.repository.CheckListRepository;
import com.wjcwleklinski.listservice.repository.EntryRepository;
import com.wjcwleklinski.listservice.service.CommonService;
import com.wjcwleklinski.listservice.service.FileService;
import com.wjcwleklinski.listservice.sync.SyncCommandHandler;
import com.wjcwleklinski.listservice.sync.SyncContext;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class EntriesSyncCommandHandler extends SyncCommandHandler<Entry, EntrySyncCommand> implements CommandHandler<EntriesSyncCommand, EntitiesSyncResponse> {

    @Getter(value = AccessLevel.PROTECTED)
    private final CommonService commonService;
    private final EntryRepository entryRepository;
    private final CheckListRepository checkListRepository;
    private final FileService fileService;

    @Override
    public EntitiesSyncResponse execute(EntriesSyncCommand command) {
        List<EntitySyncResponse> entriesSyncResponse = processEntries(command);
        return new EntitiesSyncResponse(entriesSyncResponse);
    }

    @Override
    protected Optional<Entry> findEntityInList(List<Entry> entities, EntrySyncCommand command) {
        return entities.stream()
                .filter(ch -> command.code().equals(ch.getCode()))
                .findFirst();
    }

    private Entry createEntry(EntrySyncCommand command) {
        CheckList checkList = (CheckList) commonService.getByCode(command.checkListCode(), checkListRepository);
        Entry entry = Entry.builder()
                .name(command.name())
                .description(command.description())
                .imageCode(command.imageCode())
                .priority(command.priority())
                .build();
        entry.setCheckList(checkList);
        return entry;
    }

    private Entry modifyEntry(Entry entity, EntrySyncCommand command) {
        entity.setName(command.name());
        entity.setDescription(command.description());
        entity.setImageCode(command.imageCode());
        entity.setPriority(command.priority());
        return entity;
    }

    private List<EntitySyncResponse> processEntries(EntriesSyncCommand command) {
        List<Entry> entries = loadEntries(command, entryRepository);
        SyncContext<Entry, EntrySyncCommand> syncContext = new SyncContext<>(
                entries,
                entryRepository,
                command.getEntries(),
                this::createEntry,
                this::modifyEntry,
                this::validateCreate,
                this::validateModify,
                this::validateRemoval
        );
        return synchronize(syncContext);
    }

    private List<Entry> loadEntries(EntriesSyncCommand command, EntryRepository entryRepository) {
        List<String> codes = command.getEntries().stream()
                .map(EntrySyncCommand::code)
                .toList();
        return commonService.getByCodes(codes, entryRepository);
    }

    private void validateCreate(EntrySyncCommand command) {
        commonService.getByCode(command.checkListCode(), checkListRepository);
        fileService.validateFileExistence(command.imageCode());
    }

    private void validateModify(Entry entry, EntrySyncCommand command) {
        commonService.getByCode(command.checkListCode(), checkListRepository);
        fileService.validateFileExistence(command.imageCode());
    }

    private void validateRemoval(Entry entry, EntrySyncCommand command) {
        commonService.getByCode(command.checkListCode(), checkListRepository);
    }
}
