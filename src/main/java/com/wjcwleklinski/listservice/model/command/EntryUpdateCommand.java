package com.wjcwleklinski.listservice.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wjcwleklinski.listservice.common.validation.OneOfEnum;
import com.wjcwleklinski.listservice.model.entity.Entry;
import lombok.Data;

import jakarta.validation.constraints.NotNull;

@Data
public class EntryUpdateCommand {

    @JsonIgnore
    private String entryCode;

    @JsonIgnore
    private String userId;

    @JsonIgnore
    private String listCode;

    @NotNull
    @OneOfEnum(enumClass = Entry.Priority.class)
    private String priority;

    private String name;

    private String description;

    private String imageCode;
}
