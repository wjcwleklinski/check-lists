package com.wjcwleklinski.listservice.model.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import jakarta.validation.Valid;
import java.util.List;

@Data
public class EntriesUpdateCommand {

    @JsonIgnore
    private String listCode;
    @JsonIgnore
    private String userId;
    @Valid
    private List<EntryUpsert> entries;
}
