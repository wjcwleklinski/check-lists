package com.wjcwleklinski.listservice.service;

import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.stereotype.Service;

import jakarta.servlet.http.HttpServletRequest;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class HttpService {

    private final static String DEFAULT_API_GATEWAY_URL = "localhost:8082";
    private final static String FILES_SEGMENT = "files";

    @UtilityClass
    public class Header {
        public final String X_DOMAIN_NAME = "X-Domain-Name";
    }

    private final HttpServletRequest httpServletRequest;

    private String getXDomainName() {
        return Optional.ofNullable(httpServletRequest.getHeader(Header.X_DOMAIN_NAME))
                .orElse(DEFAULT_API_GATEWAY_URL);
    }

    public String getFullFileUrl(String fileCode) {
        if (StringUtils.isBlank(fileCode)) {
            return null;
        }
        return new URIBuilder()
                .setScheme("https")
                .setHost(getXDomainName())
                .setPathSegments(FILES_SEGMENT, fileCode)
                .toString();
    }
}
