package com.wjcwleklinski.listservice.service;

import com.wjcwleklinski.listservice.model.entity.Entry;
import com.wjcwleklinski.listservice.model.view.EntryCollectionView;
import com.wjcwleklinski.listservice.model.view.EntryDetailsView;
import com.wjcwleklinski.listservice.repository.EntryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EntryQueryService {

    private final EntryRepository entryRepository;
    private final CommonService commonService;
    private final HttpService httpService;

    public List<EntryCollectionView> getEntriesInList(String listCode, String userId) {


        return entryRepository.findEntriesByCheckListCode(listCode).stream()
                .map(entry -> EntryCollectionView.getInstance(entry, httpService.getFullFileUrl(entry.getImageCode())))
                .collect(Collectors.toList());
    }

    public EntryDetailsView getEntryByCode(String productCode) {
        Entry entry = (Entry) commonService.getByCode(productCode, entryRepository);
        return EntryDetailsView.getInstance(entry, httpService.getFullFileUrl(entry.getImageCode()));
    }
}
