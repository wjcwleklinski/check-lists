package com.wjcwleklinski.listservice.service;

import com.wjcwleklinski.listservice.common.validation.CheckListValidator;
import com.wjcwleklinski.listservice.model.search.CheckListSearch;
import com.wjcwleklinski.listservice.model.specification.CheckListSpecification;
import com.wjcwleklinski.listservice.model.view.CheckListCollectionView;
import com.wjcwleklinski.listservice.model.view.CheckListDetailsView;
import com.wjcwleklinski.listservice.model.view.EntryCollectionView;
import com.wjcwleklinski.listservice.repository.CheckListRepository;
import com.wjcwleklinski.listservice.model.entity.CheckList;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CheckListService {

    private final CheckListRepository checkListRepository;
    private final CommonService commonService;
    private final HttpService httpService;
    private final CheckListValidator validator;

    public List<CheckListCollectionView> getCheckLists(String userId) {
        CheckListSearch search = CheckListSearch.builder()
                .creatorId(userId)
                .associatedUserIds(Collections.singletonList(userId))
                .build();
        CheckListSpecification spec = new CheckListSpecification(search);
        return checkListRepository.findAll(spec).stream()
                .map(projection -> CheckListCollectionView.getInstance(projection, httpService.getFullFileUrl(projection.getImageCode())))
                .collect(Collectors.toList());
    }

    public CheckListDetailsView getCheckListDetails(String listCode, String userId) {
        CheckList checkList = loadCheckList(listCode, userId);
        return CheckListDetailsView.getInstance(checkList, httpService);
    }

    public List<EntryCollectionView> getEntriesInList(String listCode, String userId) {
        CheckList checkList = loadCheckList(listCode, userId);
        return CollectionUtils.emptyIfNull(checkList.getEntries()).stream()
                .map(entry -> EntryCollectionView.getInstance(entry, httpService.getFullFileUrl(entry.getImageCode())))
                .collect(Collectors.toList());
    }

    public void deleteCheckList(String listCode, String userId) {
        loadCheckList(listCode, userId);
        commonService.deleteByCode(listCode, checkListRepository);
    }

    public CheckList loadCheckList(String listCode, String userId) {
        CheckList checkList = (CheckList) commonService.getByCode(listCode, checkListRepository);
        validator.validateUserAssignedToCheckList(userId, checkList);
        return checkList;
    }
}
