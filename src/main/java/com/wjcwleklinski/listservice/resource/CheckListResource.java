package com.wjcwleklinski.listservice.resource;

import com.wjcwleklinski.listservice.common.handler.CommandProcessor;
import com.wjcwleklinski.listservice.common.resource.CommonResource;
import com.wjcwleklinski.listservice.model.command.*;
import com.wjcwleklinski.listservice.model.view.CheckListCollectionView;
import com.wjcwleklinski.listservice.model.view.CheckListDetailsView;
import com.wjcwleklinski.listservice.service.CheckListService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/{userId}/check-lists")
@RequiredArgsConstructor
public class CheckListResource extends CommonResource {

    private final CheckListService checkListService;
    private final CommandProcessor commandProcessor;

    @GetMapping
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<List<CheckListCollectionView>> getCheckLists(@PathVariable("userId") String userId) {
        return ResponseEntity.ok(checkListService.getCheckLists(userId));
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<?> createCheckList(@RequestBody CheckListCreateCommand command,
                                             HttpServletRequest request) {
        return created(request, commandProcessor.process(command, String.class));
    }

    @GetMapping("/{listCode}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<CheckListDetailsView> getCheckList(@PathVariable("listCode") String listCode,
                                             @PathVariable("userId") String userId,
                                             @RequestHeader(name = "Host", required = false) String header) {
        CheckListDetailsView view = checkListService.getCheckListDetails(listCode, userId);
        return ResponseEntity.ok().header("Host", header).body(view);
    }

    @PutMapping("/{listCode}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<?> updateCheckList(@PathVariable("listCode") String listCode,
                                             @PathVariable("userId") String userId,
                                             @RequestBody CheckListUpdateCommand command) {
        command.setUserId(userId);
        command.setListCode(listCode);
        commandProcessor.process(command, String.class);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{listCode}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<?> deleteCheckList(@PathVariable("listCode") String listCode,
                                             @PathVariable("userId") String userId) {
        checkListService.deleteCheckList(listCode, userId);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<?> deleteCheckLists(@RequestBody CheckListsDeleteCommand command,
                                              @PathVariable("userId") String userId) {
        command.setUserId(userId);
        commandProcessor.process(command, Void.class);
        return ResponseEntity.ok().build();
    }
}
