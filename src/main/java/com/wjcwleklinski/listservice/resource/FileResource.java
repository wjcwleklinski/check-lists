package com.wjcwleklinski.listservice.resource;

import com.wjcwleklinski.listservice.model.entity.File;
import com.wjcwleklinski.listservice.model.view.FileUploadResponse;
import com.wjcwleklinski.listservice.service.FileService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequiredArgsConstructor
@RequestMapping("/files")
public class FileResource {

    private final FileService fileService;

    @PostMapping("/upload")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<?> upload(@RequestParam("file") MultipartFile file) {
        String code = fileService.storeFile(file);
        return ResponseEntity.status(HttpStatus.CREATED).body(new FileUploadResponse(code));
    }

    @GetMapping("/{fileCode}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<Resource> download(@PathVariable("fileCode") String fileCode) {
        File file = fileService.getFile(fileCode);
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(file.getType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getName() + "\"")
                .body(new ByteArrayResource(file.getData()));
    }
}
