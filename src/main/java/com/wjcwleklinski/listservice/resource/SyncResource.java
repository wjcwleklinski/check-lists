package com.wjcwleklinski.listservice.resource;

import com.wjcwleklinski.listservice.common.handler.CommandProcessor;
import com.wjcwleklinski.listservice.model.command.CheckListsSyncCommand;
import com.wjcwleklinski.listservice.model.command.EntriesSyncCommand;
import com.wjcwleklinski.listservice.model.command.FileSyncCommand;
import com.wjcwleklinski.listservice.model.command.FilesSyncCommand;
import com.wjcwleklinski.listservice.model.command.response.EntitiesSyncResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;

@RequestMapping("/{userId}/sync")
@RestController
@RequiredArgsConstructor
public class SyncResource {

    private final CommandProcessor commandProcessor;

    @PostMapping("/check-lists")
    public ResponseEntity<EntitiesSyncResponse> synchronizeCheckLists(@RequestBody @Valid CheckListsSyncCommand command,
                                                                      @PathVariable("userId") String userId) {
        command.setUserId(userId);
        EntitiesSyncResponse response = commandProcessor.process(command, EntitiesSyncResponse.class);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/entries")
    public ResponseEntity<EntitiesSyncResponse> synchronizeEntries(@RequestBody EntriesSyncCommand command,
                                                                      @PathVariable("userId") String userId) {
        command.setUserId(userId);
        EntitiesSyncResponse response = commandProcessor.process(command, EntitiesSyncResponse.class);
        return ResponseEntity.ok(response);
    }

    @PostMapping(path = "/files")
    public ResponseEntity<EntitiesSyncResponse> synchronizeFiles(@ModelAttribute FileSyncCommand fileSync,
                                                                      @PathVariable("userId") String userId) {
        FilesSyncCommand command = new FilesSyncCommand(userId, Collections.singletonList(fileSync));
        command.setUserId(userId);
        EntitiesSyncResponse response = commandProcessor.process(command, EntitiesSyncResponse.class);
        return ResponseEntity.ok(response);
    }
}
