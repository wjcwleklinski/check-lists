package com.wjcwleklinski.listservice.resource;

import com.wjcwleklinski.listservice.common.handler.CommandProcessor;
import com.wjcwleklinski.listservice.common.resource.CommonResource;
import com.wjcwleklinski.listservice.model.command.*;
import com.wjcwleklinski.listservice.model.view.EntryCollectionView;
import com.wjcwleklinski.listservice.service.CheckListService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/{userId}/check-lists/{listCode}/entries")
@RequiredArgsConstructor
@Validated
public class EntryResource extends CommonResource {

    private final CheckListService checkListService;

    private final CommandProcessor commandProcessor;

    @GetMapping
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public List<EntryCollectionView> getEntries(@PathVariable("listCode") String listCode,
                                                @PathVariable("userId") String userId) {
        return checkListService.getEntriesInList(listCode, userId);
    }

    @PatchMapping
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<?> updateEntries(@PathVariable("listCode") String listCode,
                                           @PathVariable("userId") String userId,
                                         @RequestBody @Valid EntriesUpdateCommand command) {
        command.setListCode(listCode);
        command.setUserId(userId);
        commandProcessor.process(command, Void.class);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{entryCode}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<?> updateEntry(@PathVariable("listCode") String listCode,
                                         @PathVariable("entryCode") String entryCode,
                                         @PathVariable("userId") String userId,
                                            @RequestBody EntryUpdateCommand command) {
        command.setListCode(listCode);
        command.setEntryCode(entryCode);
        command.setUserId(userId);
        commandProcessor.process(command, String.class);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/remove-selected")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<?> removeSelected(@PathVariable("listCode") String listCode,
                                            @PathVariable("userId") String userId) {
        RemoveSelectedEntriesCommand command = new RemoveSelectedEntriesCommand(listCode, userId);
        commandProcessor.process(command, String.class);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<?> deleteEntries(@PathVariable("listCode") String listCode,
                                           @PathVariable("userId") String userId,
                                           @RequestBody EntryDeleteCommand command) {
        command.setListCode(listCode);
        command.setUserId(userId);
        commandProcessor.process(command, String.class);
        return ResponseEntity.ok().build();
    }
}
