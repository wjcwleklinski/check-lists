package com.wjcwleklinski.listservice.sync;

import com.wjcwleklinski.listservice.model.command.response.EntitySyncResponse;
import com.wjcwleklinski.listservice.model.entity.CommonEntity;
import com.wjcwleklinski.listservice.service.CommonService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class SyncCommandHandler <E extends CommonEntity, C extends Syncable> {

    private static final Logger logger = LogManager.getLogger(SyncCommandHandler.class);

    protected abstract CommonService getCommonService();

    protected abstract Optional<E> findEntityInList(List<E> entities, C command);

    protected List<EntitySyncResponse> synchronize(SyncContext<E, C> context) {
        List<EntitySyncResponse> syncResponse = new ArrayList<>();
        if (CollectionUtils.isEmpty(context.commands())) {
            return syncResponse;
        }
        for (C command : context.commands()) {
            switch (command.action()) {
                case CREATION -> {
                    EntitySyncResponse response = createCommon(context, command);
                    syncResponse.add(response);
                }
                case MODIFICATION -> {
                    Optional<E> entity = findEntityInList(context.entities(), command);
                    EntitySyncResponse response =  entity
                            .map(modifiedEntity -> modifyCommon(context, modifiedEntity, command))
                            .orElseGet(() -> new EntitySyncResponse(command.code(), command.externalId(), true));
                    syncResponse.add(response);
                }
                case REMOVAL -> {
                    Optional<E> entity = findEntityInList(context.entities(), command);
                    EntitySyncResponse response =  entity
                            .map(en -> removeCommon(context, en, command))
                            .orElseGet(() -> new EntitySyncResponse(command.code(), command.externalId(), true));
                    syncResponse.add(response);
                }
            }
        }
        return syncResponse;
    }

    private EntitySyncResponse createCommon(SyncContext<E, C> context,
                                            C command) {
        try {
            context.creationValidation().accept(command);
            E createdEntity = context.creation().apply(command);
            getCommonService().save(createdEntity, context.repository());
            return new EntitySyncResponse(createdEntity.getCode(), command.externalId(), true);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new EntitySyncResponse(command.code(), command.externalId(), false);
        }
    }

    private EntitySyncResponse modifyCommon(SyncContext<E, C> context,
                                            E commonEntity,
                                            C command) {
        try {
            context.modificationValidation().accept(commonEntity, command);
            E modifiedEntity = context.modification().apply(commonEntity, command);
            getCommonService().save(modifiedEntity, context.repository());
            return new EntitySyncResponse(modifiedEntity.getCode(), command.externalId(), true);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new EntitySyncResponse(command.code(), command.externalId(), false);
        }
    }

    private EntitySyncResponse removeCommon(SyncContext<E, C> context,
                                            E commonEntity,
                                            C command) {
        try {
            context.removalValidation().accept(commonEntity, command);
            getCommonService().deleteByCode(commonEntity.getCode(), context.repository());
            return new EntitySyncResponse(commonEntity.getCode(), command.externalId(), true);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new EntitySyncResponse(command.code(), command.externalId(), false);
        }
    }
}
