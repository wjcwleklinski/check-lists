package com.wjcwleklinski.listservice.sync;


public enum SyncAction {
    CREATION,
    MODIFICATION,
    REMOVAL;
}
