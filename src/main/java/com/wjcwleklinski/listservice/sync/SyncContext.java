package com.wjcwleklinski.listservice.sync;

import com.wjcwleklinski.listservice.model.entity.CommonEntity;
import com.wjcwleklinski.listservice.repository.CommonRepository;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

public record SyncContext<E extends CommonEntity, C extends Syncable> (
    List<E> entities,
    CommonRepository<E> repository,
    List<C> commands,
    Function<C, E> creation,
    BiFunction<E, C, E> modification,
    Consumer<C> creationValidation,
    BiConsumer<E, C> modificationValidation,
    BiConsumer<E, C> removalValidation
){
}
