package com.wjcwleklinski.listservice.sync;

public interface Syncable {
    SyncAction action();
    String code();
    String externalId();
}
