package com.wjcwleklinski.listservice.security;

public interface UserAuthentication {

    String getUsername();

    String getUserId();

}
