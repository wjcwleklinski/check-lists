package com.wjcwleklinski.listservice.security;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;

@Service
public class UserAuthenticationService implements UserAuthentication {

    private final String USERNAME_CLAIM_NAME = "user_name";

    @Override
    public String getUsername() {
        return ((Jwt) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getClaimAsString(USERNAME_CLAIM_NAME);
    }

    @Override
    public String getUserId() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
}
