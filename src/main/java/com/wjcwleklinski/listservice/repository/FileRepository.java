package com.wjcwleklinski.listservice.repository;

import com.wjcwleklinski.listservice.model.entity.File;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FileRepository extends CommonRepository<File> {

    Optional<File> getFileByCode(String code);
    boolean existsByCode(String code);
}
